package com.group05.presentation;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;

/**
 * This dialog display an appropriate message at the end of the game
 * 
 * @author Alexandru Ilea
 */
@SuppressWarnings("serial")
public class EndGameDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private int result = 2;
	private byte[] userGuess;
	private JPanel contentPane;
	private JLabel lblEndGameMsg;

	/**
	 * Three parameter constructor that receives a byte array with correct
	 * answer and game result and displays it appopriately
	 * 
	 * @param userGuess
	 * @param contentPane
	 * @param gameResult
	 */
	public EndGameDialog(byte[] userGuess, JPanel contentPane, int gameResult) {
		this.userGuess = userGuess;
		this.contentPane = contentPane;
		setModalityType(ModalityType.DOCUMENT_MODAL);
		if (gameResult == 0)
			lblEndGameMsg = new JLabel("You Lost!");
		else
			lblEndGameMsg = new JLabel("You Won!");
	}

	/**
	 * Build the GUI of the JDialog
	 * 
	 */
	private void buildDialog() {

		setBounds(100, 100, 298, 209);
		setLocationRelativeTo(contentPane);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);

		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 298, 209);
		contentPanel.setBackground(new Color(229, 233, 233));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);

		lblEndGameMsg.setForeground(new Color(143, 145, 145));
		lblEndGameMsg.setHorizontalAlignment(SwingConstants.CENTER);
		lblEndGameMsg.setFont(new Font("Verdana", Font.PLAIN, 20));
		lblEndGameMsg.setBounds(68, 10, 145, 56);
		contentPanel.add(lblEndGameMsg);

		JLabel lblCorrectAnswer = new JLabel("Correct answer:");
		lblCorrectAnswer.setFont(new Font("Verdana", Font.PLAIN, 12));
		lblCorrectAnswer.setForeground(new Color(143, 145, 145));
		lblCorrectAnswer.setBounds(92, 60, 121, 15);
		contentPanel.add(lblCorrectAnswer);

		JButton okButton = new JButton("Play Again!");
		okButton.setBounds(28, 119, 115, 40);
		contentPanel.add(okButton);
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				result = 1;
				setVisible(false);
				dispose();
			}
		});
		JButton cancelButton = new JButton("Quit");
		cancelButton.setBounds(150, 119, 115, 40);
		contentPanel.add(cancelButton);
		cancelButton.setActionCommand("Cancel");
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				result = 0;
				setVisible(false);
				dispose();
			}
		});
		JPanel panel = new JPanel();
		panel.setBounds(48, 77, 200, 30);
		panel.setLayout(new GridLayout(1, 4, -1, -1));

		for (int i = 0; i < 4; i++) {

			JLabel lblGridCells = new JLabel("");
			lblGridCells.setBorder(new LineBorder(new Color(178, 181, 181), 1));
			lblGridCells.setOpaque(true);
			lblGridCells.setBackground(new Color(216, 219, 219));
			lblGridCells.setName("9"); // name out of range of 8 posible color
										// options
			if (userGuess != null) {
				int cell = userGuess[i];
				switch (cell) {
				case 0:
					lblGridCells.setBackground(new Color(166, 19, 16));
					break;
				case 1:
					lblGridCells.setBackground(new Color(26, 57, 242));
					break;
				case 2:
					lblGridCells.setBackground(new Color(243, 194, 53));
					break;
				case 3:
					lblGridCells.setBackground(new Color(243, 125, 53));
					break;
				case 4:
					lblGridCells.setBackground(new Color(133, 67, 30));
					break;
				case 5:
					lblGridCells.setBackground(new Color(140, 218, 37));
					break;
				case 6:
					lblGridCells.setBackground(new Color(42, 215, 214));
					break;
				case 7:
					lblGridCells.setBackground(new Color(221, 160, 221));
					break;
				default:
					lblGridCells.setBackground(new Color(178, 181, 181));
				}
			}
			panel.add(lblGridCells);
		}
		contentPanel.add(panel);
	}
	
	/**
	 * Calls the method to build the dialog and shows it until user hides it
	 * with one of the buttons clicks. Returns 0 if user click Quit and 1 if
	 * user clicks Play Again button.
	 * 
	 * @return results
	 */
	public int showDialog() {
		buildDialog();
		setVisible(true);
		return result;
	}
}
