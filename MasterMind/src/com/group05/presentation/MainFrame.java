package com.group05.presentation;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

import java.awt.event.InputEvent;

import javax.swing.JSeparator;
import javax.swing.border.BevelBorder;

import com.group05.client.MMClient;
import com.group05.utilities.MMPacket;

/**
 * Main game frame that manages GUI interface on client side to handle all game
 * actions.
 * 
 * @author Alexandru Ilea
 * @author Jhonatan Orjuela
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private JPanel contentPane, gameGrid, hintPanel, colorButtonPanel;
	private JButton btnSubmit, btnClear;
	private MouseListener ml = new MouseListener();
	private JToggleButton btnRed, btnOrange, btnGreen, btnBlue, btnYellow,
			btnBrown, btnCyan, btnPink;
	private JMenuItem mntmConnect, mntmNewGame, mntmQuit, mntmHowToPlay,
			mntmAbout, mntmSetCustomAnswer;

	private MMClient client;
	private MMPacket packet;
	private int gameRound = 0;
	private byte[] customAnswer = new byte[4];
	private byte[] userGuess = new byte[4];

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// set cross platform UI
		try {
			UIManager
					.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final MainFrame frame = new MainFrame();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
					// override close button of JFrame to close connection
					// before quiting the game
					frame.addWindowListener(new java.awt.event.WindowAdapter() {
						@Override
						public void windowClosing(
								java.awt.event.WindowEvent windowEvent) {
							if (frame.getTc() != null)
								frame.getTc().closeConnection();
							else
								System.exit(0);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * No parameter constructor. Sets JFrame size and calls method to draw basic
	 * GUI components.
	 */
	public MainFrame() {
		setTitle("Master Mind");
		setResizable(false);
		setBounds(100, 100, 377, 493);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(229, 233, 233));
		contentPane.setBorder(new LineBorder(new Color(211, 211, 211)));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		drawGameUI();
	}

	/**
	 * Draws basic GUI components and calls appropriate methods to draw menu,
	 * game grid, buttons and hints.
	 */
	private void drawGameUI() {
		// add title label
		JLabel lblMasterMind = new JLabel("Master Mind");
		lblMasterMind.setFont(new Font("Verdana", Font.PLAIN, 20));
		lblMasterMind.setForeground(new Color(143, 145, 145));
		lblMasterMind.setBounds(123, 6, 130, 40);
		contentPane.add(lblMasterMind);
		createMenu();
		createGameGrid();
		createColorButtons();
		createHintsPanel();

		// add submit button
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				submitGuess();
			}
		});
		btnSubmit.setBounds(87, 394, 100, 30);
		contentPane.add(btnSubmit);

		// add clear button
		btnClear = new JButton("Clear Row");
		btnClear.setBounds(186, 394, 100, 30);
		btnClear.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (gameRound > 0 && gameRound <= 10) {
					for (int i = 0; i < 4; i++) {
						gameGrid.getComponent((40 - (4 * (gameRound))) + i)
								.setBackground(Color.WHITE);
						gameGrid.getComponent((40 - (4 * (gameRound))) + i)
								.setName("9");
					}
				}
			}
		});
		contentPane.add(btnClear);
	}

	/**
	 * This method is called when user wants to start a new game. It sends start
	 * packet to server and receives server's answer.
	 */
	private void newGame() {

		gameRound = 1;
		contentPane.removeAll();
		drawGameUI();

		try {
			client.sendBytes(packet.getStart());
			selectGameAction(client.receiveBytes());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(contentPane,
					"Server not answering...");
		}
	}

	/**
	 * Receives an array of bytes and decides on the action according to the
	 * message received.
	 * 
	 * @param receiveBytes
	 * @param userGuess
	 */
	private void selectGameAction(byte[] receiveBytes) {
		// start game
		if (Arrays.equals(receiveBytes, packet.getStart())) {
			for (int i = 0; i < 40; i++) {
				gameGrid.getComponent(i)
						.setBackground(new Color(255, 250, 250));
				if (i > 35)
					gameGrid.getComponent(i).addMouseListener(ml);
			}
			// start game with custom answer
		} else if (Arrays.equals(receiveBytes,
				packet.getStartWithCustomAnswer())) {
			try {
				client.sendBytes(customAnswer);
				selectGameAction(client.receiveBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			// game lost
		} else if (Arrays.equals(receiveBytes, packet.getLose())) {
			byte[] correctAnswer = new byte[4];

			try {
				correctAnswer = client.receiveBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}

			EndGameDialog dialog = new EndGameDialog(correctAnswer,
					contentPane, 0);

			if (dialog.showDialog() == 1)
				newGame();
			else {
				if (client != null)
					client.closeConnection();
				else
					System.exit(0);
			}
			// game won
		} else if (Arrays.equals(receiveBytes, packet.getWin())) {

			EndGameDialog dialog = new EndGameDialog(userGuess, contentPane, 1);
			if (dialog.showDialog() == 1)
				newGame();
			else {
				if (client != null)
					client.closeConnection();
				else
					System.exit(0);
			}
			// receive clues
		} else {

			String clues = client.decodeClues(receiveBytes);
			JLabel jk = (JLabel) hintPanel.getComponent(10 - gameRound);
			jk.setText(clues);
			gameRound++;
		}

	}

	/**
	 * Runs when user clicks submit button Assigns values of the current row to
	 * a byte array. Sends the message to the server, receives the answer and
	 * passes it to appropriate method.
	 */
	private void submitGuess() {

		if (gameRound > 0 && gameRound <= 10) {
			boolean filled = true;

			for (int i = 0; i < 4; i++) {
				int cellNum = Integer.parseInt(gameGrid.getComponent(
						(40 - (4 * gameRound)) + i).getName());
				if (cellNum > 8)
					filled = false;
			}

			if (filled == true) {
				for (int i = 0; i < 4; i++) {
					userGuess[i] = Byte.parseByte(gameGrid.getComponent(
							(40 - (4 * gameRound)) + i).getName());
					gameGrid.getComponent((40 - (4 * (gameRound))) + i)
							.removeMouseListener(ml);
					if (gameRound < 10)
						gameGrid.getComponent((40 - (4 * (gameRound + 1))) + i)
								.addMouseListener(ml);
				}

				try {
					client.sendBytes(userGuess);
					selectGameAction(client.receiveBytes());

				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				JOptionPane.showMessageDialog(contentPane,
						"Please fill all cells on row #" + gameRound + "!",
						"Empty cells!", JOptionPane.WARNING_MESSAGE);
			}
		}

	}

	/**
	 * This method creates a game grid of 10 rows and 4 columns
	 */
	private void createGameGrid() {

		// add grid 10 by 4 grid for user choices
		gameGrid = new JPanel();
		gameGrid.setBackground(new Color(216, 219, 219));
		gameGrid.setBounds(65, 72, 200, 250);
		gameGrid.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		gameGrid.setLayout(new GridLayout(10, 4, -1, -1));

		for (int i = 0; i < 4 * 10; i++) {
			JLabel lblGridCells = new JLabel("");
			lblGridCells.setBorder(new LineBorder(new Color(178, 181, 181), 1));
			lblGridCells.setOpaque(true);
			lblGridCells.setBackground(new Color(216, 219, 219));
			lblGridCells.setName("9"); // name out of range of 8 posible color
										// options

			gameGrid.add(lblGridCells);
		}

		JSeparator separator = new JSeparator();
		separator.setBounds(30, 44, 320, 2);
		contentPane.add(separator);
		contentPane.add(gameGrid);

		// add a panel with 10 nums corresponding to rounds
		JPanel roundNumPanel = new JPanel();
		roundNumPanel.setBounds(45, 72, 20, 250);
		roundNumPanel.setLayout(new GridLayout(10, 1));
		roundNumPanel.setBackground(new Color(216, 219, 219));

		for (int i = 10; i > 0; i--) {
			JLabel roundNums = new JLabel(i + "");
			roundNums.setOpaque(true);
			roundNums.setForeground(new Color(143, 145, 145));
			roundNums.setBackground(new Color(229, 233, 233));
			roundNumPanel.add(roundNums);
		}
		contentPane.add(roundNumPanel);
	}

	/**
	 * Create GUI for color selection butttons
	 */
	private void createColorButtons() {

		ButtonGroup buttonGroup = new ButtonGroup();
		colorButtonPanel = new JPanel();
		colorButtonPanel.setBackground(new Color(216, 219, 219));
		colorButtonPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null,
				null, null, null));
		colorButtonPanel.setBounds(44, 343, 285, 36);
		contentPane.add(colorButtonPanel);
		colorButtonPanel.setLayout(new GridLayout(1, 8));

		btnRed = new JToggleButton("");
		buttonGroup.add(btnRed);
		btnRed.setSelected(true);
		btnRed.setBackground(new Color(166, 19, 16));
		colorButtonPanel.add(btnRed);

		btnBlue = new JToggleButton("");
		buttonGroup.add(btnBlue);
		btnBlue.setBackground(new Color(26, 57, 242));
		colorButtonPanel.add(btnBlue);

		btnYellow = new JToggleButton("");
		buttonGroup.add(btnYellow);
		btnYellow.setBackground(new Color(243, 194, 53));
		colorButtonPanel.add(btnYellow);

		btnOrange = new JToggleButton("");
		buttonGroup.add(btnOrange);
		btnOrange.setBackground(new Color(243, 125, 53));
		colorButtonPanel.add(btnOrange);

		btnBrown = new JToggleButton("");
		buttonGroup.add(btnBrown);
		btnBrown.setBackground(new Color(133, 67, 30));
		colorButtonPanel.add(btnBrown);

		btnGreen = new JToggleButton("");
		buttonGroup.add(btnGreen);
		btnGreen.setBackground(new Color(140, 218, 37));
		colorButtonPanel.add(btnGreen);

		btnCyan = new JToggleButton("");
		buttonGroup.add(btnCyan);
		btnCyan.setBackground(new Color(42, 215, 214));
		colorButtonPanel.add(btnCyan);

		btnPink = new JToggleButton("");
		buttonGroup.add(btnPink);
		btnPink.setBackground(new Color(221, 160, 221));
		colorButtonPanel.add(btnPink);

	}

	/**
	 * Create GUI for game menu
	 */
	private void createMenu() {

		MenuActionListener mal = new MenuActionListener();

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		mntmConnect = new JMenuItem("Connect");
		mntmConnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,
				InputEvent.ALT_MASK));
		if (client != null)
			mntmConnect.setEnabled(false);
		mnFile.add(mntmConnect);
		mntmConnect.addActionListener(mal);

		mntmNewGame = new JMenuItem("New Game");
		mntmNewGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				ActionEvent.ALT_MASK));
		mntmNewGame.addActionListener(mal);
		if (client == null)
			mntmNewGame.setEnabled(false);
		mnFile.addSeparator();
		mnFile.add(mntmNewGame);

		mntmQuit = new JMenuItem("Quit");
		mntmQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
				ActionEvent.ALT_MASK));
		mntmQuit.addActionListener(mal);
		mnFile.add(mntmQuit);

		JMenu mnTools = new JMenu("Tools");
		menuBar.add(mnTools);

		mntmSetCustomAnswer = new JMenuItem("Custom answer set");
		if (client == null)
			mntmSetCustomAnswer.setEnabled(false);
		mntmSetCustomAnswer.setAccelerator(KeyStroke.getKeyStroke(
				KeyEvent.VK_S, ActionEvent.ALT_MASK));
		mntmSetCustomAnswer.addActionListener(mal);
		mnTools.add(mntmSetCustomAnswer);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		mntmHowToPlay = new JMenuItem("How to play?");
		mnHelp.add(mntmHowToPlay);
		mntmHowToPlay.addActionListener(mal);
		mntmHowToPlay.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H,
				ActionEvent.ALT_MASK));

		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(mal);
		mntmAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,
				ActionEvent.ALT_MASK));
		mnHelp.add(mntmAbout);

	}

	/**
	 * Create GUI for hints panel
	 */
	private void createHintsPanel() {

		hintPanel = new JPanel();
		hintPanel.setBounds(269, 72, 65, 250);
		hintPanel.setBackground(new Color(216, 219, 219));
		hintPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null,
				null, null));
		hintPanel.setLayout(new GridLayout(10, 1, 0, 0));

		for (int i = 9; i > -1; i--) {
			JLabel lblHints = new JLabel("");
			lblHints.setFont(new Font("Dialog", Font.BOLD, 16));
			lblHints.setName(i + "");
			hintPanel.add(lblHints);
		}

		contentPane.add(hintPanel);

	}

	/**
	 * Inner class that manages mouse clicks on grid cells
	 * 
	 * @author Alexandru Ilea
	 * @author Jhonatan Orjuela
	 * 
	 */
	class MouseListener extends MouseAdapter {

		public void mousePressed(MouseEvent e) {

			if (btnRed.isSelected()) {
				e.getComponent().setBackground(new Color(166, 19, 16));
				e.getComponent().setName("0");
			} else if (btnBlue.isSelected()) {
				e.getComponent().setBackground(new Color(26, 57, 242));
				e.getComponent().setName("1");
			} else if (btnYellow.isSelected()) {
				e.getComponent().setBackground(new Color(243, 194, 53));
				e.getComponent().setName("2");
			} else if (btnOrange.isSelected()) {
				e.getComponent().setBackground(new Color(243, 125, 53));
				e.getComponent().setName("3");
			} else if (btnBrown.isSelected()) {
				e.getComponent().setBackground(new Color(133, 67, 30));
				e.getComponent().setName("4");
			} else if (btnGreen.isSelected()) {
				e.getComponent().setBackground(new Color(140, 218, 37));
				e.getComponent().setName("5");
			} else if (btnCyan.isSelected()) {
				e.getComponent().setBackground(new Color(42, 215, 214));
				e.getComponent().setName("6");
			} else if (btnPink.isSelected()) {
				e.getComponent().setBackground(new Color(221, 160, 221));
				e.getComponent().setName("7");
			}

		}

	}

	/**
	 * Inner class that manages menu clicks
	 * 
	 * @author Alexandru Ilea
	 * @author Jhonatan Orjuela
	 */
	class MenuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// connect menu
			if (e.getSource() == mntmConnect) {
				String serverIp = JOptionPane.showInputDialog(contentPane,
						"Please input server IP address!", "Connect",
						JOptionPane.QUESTION_MESSAGE);

				try {
					client = new MMClient(serverIp);
					packet = new MMPacket();
					mntmNewGame.setEnabled(true);
					mntmSetCustomAnswer.setEnabled(true);
					mntmConnect.setEnabled(false);
					JOptionPane.showMessageDialog(contentPane,
							"Connection succeed! Press 'New Game' to begin!");
				} catch (IOException e1) {
					mntmConnect.setEnabled(true);
					JOptionPane
							.showMessageDialog(contentPane,
									"Connection failed! Please input correct server IP address!");
				}
				// new game menu
			} else if (e.getSource() == mntmNewGame) {
				if (client != null)
					newGame();
				// quit menu
			} else if (e.getSource() == mntmQuit) {
				if (client != null)
					client.closeConnection();
				else
					System.exit(0);
				// how to play menu
			} else if (e.getSource() == mntmHowToPlay) {
				JOptionPane
						.showMessageDialog(
								contentPane,
								"How to play:\n\n"
										+ "1. Connect to the server \n"
										+ "2. Start new game \n"
										+ "3. Try to guess a set of four random colours\n"
										+ "4. Press on a cell to select its colour\n"
										+ "5. You have 10 tries, after every try you receive a set of hints\n"
										+ "6. Empty dot = one cell is at right place and of the right colour\n"
										+ "7. A filled dot = the colour is part of the guessed set but not in the right position.",
								"How to play?", JOptionPane.INFORMATION_MESSAGE);
				// about menu
			} else if (e.getSource() == mntmAbout) {
				JOptionPane
						.showMessageDialog(contentPane,
								"Copyright 2013 - Master Mind Game\nDevelopped by: Alex I. Anna S. Jhonatan O.");
				// custom answer menu
			} else if (e.getSource() == mntmSetCustomAnswer) {

				String input = "0000";
				input = JOptionPane
						.showInputDialog(
								contentPane,
								"Please enter a custom answer set (values 0-7):\nDefault:0000 if wrong input.",
								"0000");

				if ((input != null) && (input.length() > 0)) {
					input = input.substring(0, 4);

					for (int i = 0; i < 4; i++) {
						if (Integer.parseInt(input.substring(i, i + 1)) < 8)
							customAnswer[i] = (byte) Integer.parseInt(input
									.substring(i, i + 1));
						else
							customAnswer[i] = 0;
					}
					gameRound = 1;
					contentPane.removeAll();
					drawGameUI();
					try {
						client.sendBytes(packet.getStartWithCustomAnswer());
						selectGameAction(client.receiveBytes());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

			}
		}
	}

	/**
	 * @return intance of the client class
	 */
	public MMClient getTc() {
		return client;
	}
}