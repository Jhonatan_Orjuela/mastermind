package com.group05.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;

/**
 * Utility class for the MasterMind packets. Holds special communication packets
 * (start, end, win, lose game). Designates the bytes that represent type or
 * position matches.
 * 
 * @author Ganna Shmatova
 * @author Alexander Ilea
 * @version 1.0
 */
public class MMPacket {
	private static final int byteSize = 4;
	private byte typeMatches = 0xC;
	private byte placeMatches = 0xB;

	private byte[] start = { 0xF, 0xF, 0xF, 0xF };
	private byte[] startWithCustomAnswer = { 0xF, 0xF, 0xF, 0xE };
	private byte[] end = { 0xE, 0xE, 0xE, 0xE };
	private byte[] win = { placeMatches, placeMatches, placeMatches,
			placeMatches };
	private byte[] lose = { 0xD, 0xD, 0xD, 0xD };

	/**
	 * Utility method that reads the given InputStream until it gathers a
	 * predefined length array of bytes, since each message mastermind sends is
	 * the same size of bytes.
	 * 
	 * @param in
	 *            InputStream that is read until the designated byteSize is
	 *            filled.
	 * @return set of bytes with the whole message received
	 * @throws IOException
	 *             if stream ends unexpectedly
	 */
	public byte[] getResponse(InputStream in) throws IOException {
		byte[] msg = new byte[byteSize];
		int bytesRcvd, totalBytesRcvd;
		bytesRcvd = totalBytesRcvd = 0;
		while (totalBytesRcvd < msg.length) {
			if ((bytesRcvd = in.read(msg, totalBytesRcvd, msg.length
					- totalBytesRcvd)) == -1)
				throw new SocketException("Connection lost");
			totalBytesRcvd += bytesRcvd;
		}
		return msg;
	}

	/**
	 * @return set of bytes representing a start game message.
	 */
	public byte[] getStart() {
		return start;
	}

	/**
	 * @return set of bytes representing start game and that client will provide
	 *         answer.
	 */
	public byte[] getStartWithCustomAnswer() {
		return startWithCustomAnswer;
	}

	/**
	 * @return set of bytes representing a win game message.
	 */
	public byte[] getWin() {
		return win;
	}

	/**
	 * @return set of bytes representing a lose game message.
	 */
	public byte[] getLose() {
		return lose;
	}

	/**
	 * @return set of bytes representing an end game message.
	 */
	public byte[] getEnd() {
		return end;
	}

	/**
	 * @return byte representing a type match
	 */
	public byte getTypeMatches() {
		return typeMatches;
	}

	/**
	 * @return byte representing a position/place match
	 */
	public byte getPlaceMatches() {
		return placeMatches;
	}

	/**
	 * @return int length of bytes
	 */
	public int getByteSize() {
		return byteSize;
	}

}
