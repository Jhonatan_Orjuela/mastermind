package com.group05.client;

import java.net.*; // for Socket
import java.io.*; // for IOException and Input/OutputStream

import com.group05.utilities.MMPacket;

/**
 * Client class that creates a connection to the server, sends and receives
 * arrays of bytes and decodes clues for use in the GUI interface
 * 
 * @author Alex Ilea
 * @author Jhonatan Orjuela
 */
public class MMClient {

	private int servPort;
	private Socket socket;
	private MMPacket packet;

	/**
	 * One parameter constructor, that receives a server IP and creates a socket
	 * connection if the server ip is valid. Timeouts after 3 seconds.
	 * 
	 * @param serverIp
	 * @throws IOException
	 */
	public MMClient(String serverIp) throws IOException {

		packet = new MMPacket();
		servPort = 50000;
		socket = new Socket();
		socket.connect(new InetSocketAddress(serverIp, servPort), 3000);
		System.out.println("Connected to server!");
	}

	/**
	 * Closes socket connection and exits the game
	 */
	public void closeConnection() {
		try {
			sendBytes(packet.getEnd());
			socket.close();
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends and array of bytes to the server
	 * 
	 * @param byteBuffer
	 * @throws IOException
	 */
	public void sendBytes(byte[] byteBuffer) throws IOException {
		socket.getOutputStream().write(byteBuffer); // Send the encoded string
													// to the server
	}

	/**
	 * Receives array of bytes from the server using utility method from
	 * MMPacket class Returns received bytes.
	 * 
	 * @return byte [] receivedBytes
	 * @throws IOException
	 */
	public byte[] receiveBytes() throws IOException {
		InputStream in = socket.getInputStream();
		MMPacket mmp = new MMPacket();
		return mmp.getResponse(in);
	}

	/**
	 * Utility method that decodes byte array of clues and returns a string
	 * representation of array to be used in GUI
	 * 
	 * @param byte [] clues
	 * @return String clueString
	 */
	public String decodeClues(byte[] clues) {
		String clueString = "";

		for (int i = 0; i < clues.length; i++) {
			if (clues[i] == packet.getTypeMatches())
				clueString += "\u2022 ";
			else if (clues[i] == packet.getPlaceMatches())
				clueString += "\uFFEE ";
		}
		return clueString;
	}

}