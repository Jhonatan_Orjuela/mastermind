package com.group05.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

import com.group05.utilities.MMPacket;

/**
 * Session class is responsible for communicating information with the client.
 * It can start or restart a game, keep track of rounds, and generate answers and hints.
 * @author Ganna Shmatova
 * @version 1.1
 */
public class MMSession implements Runnable{
	private MMPacket packet;
	private Socket clientSocket;
	
	private int possibleAnswers;
	private int totalRounds;
	
	/**
	 * Instantiates class variables.
	 * @param clientSocket that this session will communicate with.
	 */
	public MMSession(Socket clientSocket) {
		this.clientSocket = clientSocket;
		totalRounds = 10;
		possibleAnswers = 8;
		packet = new MMPacket();
	}
	
	/**
	 * Does the game loop. Handles speaking with the client.
	 */
	public void run() {
		try{
			boolean listening = true;
			int round = 1;
			InputStream in = clientSocket.getInputStream();
			OutputStream out = clientSocket.getOutputStream();
			
			byte[] msg;
			byte[] answer = new byte[packet.getByteSize()];
			
			while (listening) {
				System.out.println("Waiting for client's response...");
				msg = packet.getResponse(in); // gets response from client
				printArray("\tGot response: ", msg);
	
				if (Arrays.equals(packet.getStart(), msg)) { // client starts a new game
					round = 1;
					answer = generateAnswer();
					printArray("Generated answer: ", answer);
					out.write(packet.getStart());
				} else if (Arrays.equals(packet.getStartWithCustomAnswer(), msg)){
					round = 1;
					out.write(packet.getStartWithCustomAnswer());//echo
					answer = packet.getResponse(in); //get
					printArray("Given answer: ", answer);
					out.write(packet.getStart());
				} else if (Arrays.equals(packet.getEnd(), msg)) { // client ends game
					System.out.println("Exits game loop");
					listening = false;
				} else if(round >= totalRounds && !(Arrays.equals(answer, msg))){ //if out of rounds
					out.write(packet.getLose()); //sends notification that you've lost the game to client
					System.out.println("Sent game lost packet");
					out.write(answer);
					printArray("Sent back answer: ", answer);
				} else { // if not pre-defined packet, must be the user's guesses, so server generates hints in reply
					System.out.println("Round " + round + " hints:");
					byte[] hints = generateHints(answer, msg); //generates hints from client's guesses
					out.write(hints); // sends hints back
					printArray("Sent hints: ", hints);
					round++;
				}
			}
			clientSocket.close(); // Close the socket. This client is finished.
			System.out.println("Socket closed\n");
		}catch(IOException ieo){
			ieo.printStackTrace();
		}
	}
	
	/**
	 * Given answer and guess, generates the position and type hint
	 * array of bytes, following MMPacket's variables.
	 * @param answer The real answer.
	 * @param guess The guessed answer that will receive the hints.
	 * @return byte array of position and type hints.
	 */
	public byte[] generateHints(byte[] answer, byte[] guess) {
		byte[] hintArray = new byte[packet.getByteSize()];
		byte[] answerTypeCount = new byte[possibleAnswers];
		byte[] guessTypeCount = new byte[possibleAnswers];
		
		//fills counters
		for(int i=0; i<answer.length; i++)
			answerTypeCount[answer[i]]++;
		for(int i=0; i<guess.length; i++)
			guessTypeCount[guess[i]]++;
		
		int typeMatches = 0, posMatches = 0;
		//finds position matches
		for(int i=0; i<answer.length; i++){
			if(answer[i] == guess[i]){
				posMatches++;
				//lowers counters
				answerTypeCount[answer[i]]--;
				guessTypeCount[guess[i]]--;
			}
		}
		//finds other type matches
		int smallestCommonNum;
		for(int i=0; i<answerTypeCount.length; i++){
			if(answerTypeCount[i] > guessTypeCount[i]) //gets the smallest number both lists can be subtracted vs without going past 0
				smallestCommonNum =  guessTypeCount[i];
			else
				smallestCommonNum = answerTypeCount[i];
			typeMatches+=smallestCommonNum;
		}
		
		System.out.println("\tPos match: " + posMatches);
		System.out.println("\tType match: " + typeMatches);
		
		//populates hint array
		for(int i=0; i < posMatches; i++)
			hintArray[i] = packet.getPlaceMatches();
		for(int i=0; i < typeMatches; i++)
			hintArray[posMatches + i] = packet.getTypeMatches();
		return hintArray;
	}
	
	
	/**
	 * Randomly generates a byte array that has the numbers 0 to a predefined
	 * maximum possible answer number.
	 * @return byte array of randomly generates answers.
	 */
	private byte[] generateAnswer() {
		byte[] answer = new byte[packet.getByteSize()];
		for (int i = 0; i < answer.length; i++)
			answer[i] = (byte) ((Math.random() * possibleAnswers));
		return answer;
	}
	
	/**
	 * Prints array in a nice format with a title heading.
	 * @param msg
	 * @param bs
	 */
	private void printArray(String msg, byte[] bs){
		System.out.print(msg);
		for(Byte b: bs)
			System.out.print(b + " ");
		System.out.println();
	}
}
