package com.group05.server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Server class. By convention you need to call the neverending loop for client
 * handling. In the current state, if a client connects the server stops
 * listening.
 * 
 * @author Ganna Shmatova
 * @author Alexander Ilea
 * @version 1.2
 */

public class MMServer {
	private int servPort = 50000;

	/**
	 * Instantiates server and starts listen loop.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		MMServer ts = new MMServer();
		ts.listen();
	}

	/**
	 * Listen loop. The server waits for a client, if a client connects it
	 * starts a session in a new thread.
	 */
	public void listen() {
		ServerSocket servSock;
		try {
			while (true) {
				servSock = new ServerSocket(servPort);
				showMyIp();
				System.out.println("Listening for new client connection...");
				Socket clntSock = servSock.accept(); // wait until a client
														// connects
				System.out.println("Handling client at "
						+ clntSock.getInetAddress().getHostAddress()
						+ " on port " + clntSock.getPort());
				MMSession mss = new MMSession(clntSock);
				new Thread(mss).start();
				servSock.close();
			}
		} catch (IOException ioe) {
			System.out.println("Crash!");
			ioe.printStackTrace();
		}
	}

	/**
	 * Prints the server's network interfaces and their respective addresses.
	 */
	private void showMyIp() {
		try {
			String ip = InetAddress.getLocalHost().getHostAddress();
			System.out.println("Server IP address: " + ip);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
