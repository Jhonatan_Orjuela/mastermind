package com.g3w14.action;

import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.g3w14.data.ClientBean;
import com.g3w14.persistence.ClientDAOImp;


/**
 * Action Bean that handles signing a user in to the website after
 * they enter their credentials.
 * 
 * @author Theo de villeneuve, Sean-Frankel Gaon Canlas
 *
 */

@Named("loginBean")
@SessionScoped
public class LoginBean implements Serializable
{
	private String password;
	private String email;
	private int id;
	private boolean logedin = false;
	private boolean admin = false;
	private static final long serialVersionUID = 1L;
	@Inject
	private ClientBean clientBean;
	@Inject
	private ClientDAOImp clientdao;

	// Méthode d'action appelée lors du clic sur le bouton du formulaire
	// d'inscription
	public LoginBean(){
		clientBean = new ClientBean();
	}
	public void login() throws IOException, NoSuchAlgorithmException,
	InvalidKeySpecException, SQLException {
		if(clientdao.logionSuccess(email, password)){
			logedin = true;
			clientBean = clientdao.getQuerySingleClient(email);
			id = clientBean.getId();
			if(clientBean.getAdmin() == 1){
				admin = true;
			}
			
			FacesMessage message = new FacesMessage( "Welcome " + clientBean.getFirstName() );
			FacesContext.getCurrentInstance().addMessage( "loginmsg", message );
		}
		else{
			logedin = false;
			FacesMessage message = new FacesMessage( "login failed ! Try again ..." );
			FacesContext.getCurrentInstance().addMessage( "loginmsg", message );
		}
	}
	public void logout(){
		logedin = false;
		admin = false;
		FacesMessage message = new FacesMessage( "GoodBye " );
		FacesContext.getCurrentInstance().addMessage( "loginmsg", message );
	}
	
	public ClientBean getClientBean() {
		return clientBean;
	}
	
	public void setClientBean(ClientBean clientBean) {
		this.clientBean = clientBean;
	}	
	
	/**
	 * @return the logedin
	 */
	public boolean isLogedin() {
		return logedin;
	}
	/**
	 * @param logedin the logedin to set
	 */
	public void setLogedin(boolean logedin) {
		this.logedin = logedin;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}
	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



}