/**
 * 
 */
package com.g3w14.action;

import java.awt.event.ActionEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.g3w14.data.AdvertisementBean;
import com.g3w14.data.BookBean;
import com.g3w14.data.ClientBean;
import com.g3w14.data.CustomerReviewBean;
import com.g3w14.data.ShippingInfoBean;
import com.g3w14.data.SurveyBean;
import com.g3w14.persistence.AdvertisementDAOImp;
import com.g3w14.persistence.BookDAOImp;
import com.g3w14.persistence.ClientDAOImp;
import com.g3w14.persistence.CustomerReviewDAOImp;
import com.g3w14.persistence.ShippingInfoDAOImp;
import com.g3w14.persistence.SurveyDAOImp;

/**
 * @author Théo de villeneuve
 *
 */
@Named ("managementBean")
@SessionScoped
public class ManagementBean implements Serializable {
	private String vide = "" ;
	private Date firstDate;
	private Date secondDate;
	private String Datetransf ;
	private String Datetransf2;
	public void convertDate(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.S");
		if(firstDate.before(secondDate)){
			Date date = firstDate;
			Datetransf = formatter.format(date);
			date = secondDate;
			Datetransf2 = formatter.format(date);		
		}else{
			Datetransf = "2014/03/31 00:00:00.0";
			Datetransf2 = "2014/03/03 00:00:00.0";
		}
	}
	public void newBook(BookBean book) throws SQLException{
		ArrayList<UploadedImage> list = upload.getFiles();
		book.setBookCoverImg(list.get(0).getName());
		bookdao.insertRecord(book);
	}
	public void newAd(AdvertisementBean ad) throws SQLException{
		ArrayList<UploadedImage> list = upload.getFiles();
		ad.setAdFileName(list.get(0).getName());
		addao.insertRecord(ad);
	}
	public void newShippingInfo(ShippingInfoBean ship) throws SQLException{
		ship.setClientid(login.getId());
		
		System.out.println("first name: " + ship.getShipFirstName());
		
		shipdao.insertShippingInfo(ship);		
	}
	
	public String getBookName(String isbn) throws SQLException{
		String name = bookdao.getQuerySingleBook(isbn).getTitle();
		return name;
	}
	public String getClientName(int id) throws SQLException{
		String name = clientdao.getQuerySingleClientId(id).getLastName();
		name += " " + clientdao.getQuerySingleClientId(id).getFirstName();
		return name;
	}
	public BigDecimal getTotal(int quantity, BigDecimal price){
		BigDecimal bd = new BigDecimal(String.valueOf(quantity));
		BigDecimal total = bd.multiply(price);
		return total;		
	}

	public void approval(int key) throws SQLException{
		CustomerReviewBean review = reviewdao.getSingleReview(key);
		if(review.getApproval() ==  true){
			review.setApproval(false);
			reviewdao.updateRecord(review);
		}
		else{
			review.setApproval(true);
			reviewdao.updateRecord(review);
		}
	}
	public void displayAd(int id) throws SQLException{
		AdvertisementBean ad = addao.getSpecificQueryRecordById(id);
		if(ad.isDisplay()){
			ad.setDisplay(false);
			addao.updateRecord(ad);
			
		}else{
			ad.setDisplay(true);
			addao.updateRecord(ad);
		}

	}
	public void displaySurvey(int id) throws SQLException{
		SurveyBean survey = surveydao.getSingleSurvey(id);
		ArrayList<SurveyBean> surveylist = surveydao.getSurveyRecords();
		if(survey.getDisable() == true){
			for(SurveyBean s : surveylist){
				if(! s.getDisable()){
					s.setDisable(true);
					surveydao.updateSurvey(s);
				}
			}
			survey.setDisable(false);
			surveydao.updateSurvey(survey);
			
		}else{
			survey.setDisable(true);
			surveydao.updateSurvey(survey);	
		}

	}
	private ClientBean editclient;
	/**
	 * @return the editclient
	 */
	public ClientBean getEditclient() {
		return editclient;
	}

	/**
	 * @param editclient the editclient to set
	 */
	public void setEditclient(ClientBean editclient) {
		this.editclient = editclient;
	}
	private BookBean editbook;
	@Inject
	BookBean currentbookbean;
	@Inject
	BookDAOImp bookdao;
	@Inject
	FileUploadBean upload;
	@Inject
	ClientDAOImp clientdao;
	@Inject
	CustomerReviewDAOImp reviewdao;
	@Inject
	SurveyDAOImp surveydao;
	@Inject
	LoginBean login;
	@Inject
	ShippingInfoDAOImp shipdao;
	@Inject
	AdvertisementDAOImp addao;
	/**
	 * 
	 */
	private ShippingInfoBean ship;
	private static final long serialVersionUID = 1L;
	public void findclient(ComponentSystemEvent event) throws SQLException{
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String bookId = request.getParameter("user");
		editclient = clientdao.getQuerySingleClient(bookId);
	}	
	public void findbook(ComponentSystemEvent event) throws SQLException{
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String bookId = request.getParameter("book");
		editbook = bookdao.getQuerySingleBook(bookId);
	}
	public void findShipInfo(ComponentSystemEvent event) throws SQLException{
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String bookId = request.getParameter("ship");
		System.out.println(bookId);
		ship = shipdao.searchShipInfoById(Integer.parseInt(bookId));
	}
	
	//Return the email user param
	public String getUserParam(){
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String bookId = request.getParameter("user");
		return bookId;
	}
	public boolean isEbookFormat(String isbn13) throws SQLException{
		BookBean book = bookdao.getQuerySingleBook(isbn13);
		if(book.getBookType() == 3){
			return true;
		}else{
			return false;
		}
	}

	public BigDecimal getWholeSalePriceInventory() throws SQLException{
		ArrayList<BookBean> book = new ArrayList<>();
		book = bookdao.getAllRecords();
		BigDecimal total = new BigDecimal("0");
		for (int i = 0; i < book.size(); i++) {
			BigDecimal bd = new BigDecimal(String.valueOf(book.get(i).getNumOfCopies()));
			total = total.add(bd.multiply(book.get(i).getWholeSalePrice()));
		}
		return total;
	}

	public BigDecimal getListPriceInventory() throws SQLException{
		ArrayList<BookBean> book = new ArrayList<>();
		book = bookdao.getAllRecords();
		BigDecimal total = new BigDecimal("0");
		for (int i = 0; i < book.size(); i++) {
			BigDecimal bd = new BigDecimal(String.valueOf(book.get(i).getNumOfCopies()));
			total = total.add(bd.multiply(book.get(i).getListPrice()));
		}
		return total;
	}

	public int getNumberOfBook() throws SQLException{
		ArrayList<BookBean> book = new ArrayList<>();
		book = bookdao.getAllRecords();
		int total = 0;
		for (int i = 0; i < book.size(); i++) {
			total += book.get(i).getNumOfCopies();
		}
		return total;
	}

	public String editClient() throws SQLException{
		System.out.println(" managementBean --> UPDATEclient");
		System.out.println(editclient.getCountry());
		clientdao.updateClient(editclient);	
		return "management.xhtml";
	}
	public String editBook() throws SQLException{
		System.out.println(" managementBean --> UPDATEBook");
		System.out.println(editbook.toString());
		bookdao.updateRecord(editbook);		
		return "management.xhtml";
	}
	public String editShip() throws SQLException{
		System.out.println(" managementBean --> UPDATEdSHIP");
		System.out.println(ship.toString());
		shipdao.updateShipInfo(ship);		
		return "account.xhtml";
	}
	
	public void validateAccount(boolean logedin, boolean admin) throws IOException {
		if(!logedin && !admin) {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().responseSendError(404, "");
	        fc.responseComplete();
		}
		else if(!admin) {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().responseSendError(404, "");
	        fc.responseComplete();
		}
		else if(!logedin) {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().responseSendError(404, "");
	        fc.responseComplete();
		}
	}
	
	/**
	 * @return the firstDate
	 */
	public Date getFirstDate() {
		return firstDate;
	}
	/**
	 * @param firstDate the firstDate to set
	 */
	public void setFirstDate(Date firstDate) {
		this.firstDate = firstDate;
	}
	/**
	 * @return the secondDate
	 */
	public Date getSecondDate() {
		return secondDate;
	}
	/**
	 * @param secondDate the secondDate to set
	 */
	public void setSecondDate(Date secondDate) {
		this.secondDate = secondDate;
	}

	/**
	 * @return the datetransf
	 */
	public String getDatetransf() {
		return Datetransf;
	}

	/**
	 * @param datetransf the datetransf to set
	 */
	public void setDatetransf(String datetransf) {
		Datetransf = datetransf;
	}

	/**
	 * @return the datetransf2
	 */
	public String getDatetransf2() {
		return Datetransf2;
	}

	/**
	 * @param datetransf2 the datetransf2 to set
	 */
	public void setDatetransf2(String datetransf2) {
		Datetransf2 = datetransf2;
	}

	/**
	 * @return the editbook
	 */
	public BookBean getEditbook() {
		return editbook;
	}

	/**
	 * @param editbook the editbook to set
	 */
	public void setEditbook(BookBean editbook) {
		this.editbook = editbook;
	}
	/**
	 * @return the ship
	 */
	public ShippingInfoBean getShip() {
		return ship;
	}
	/**
	 * @param ship the ship to set
	 */
	public void setShip(ShippingInfoBean ship) {
		this.ship = ship;
	}
	/**
	 * @return the vide
	 */
	public String getVide() {
		return vide;
	}




}
