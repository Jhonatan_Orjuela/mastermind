package com.g3w14.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.InvoiceBean;

/**
 * Implementation class that enables the ability to manipulate and query records from 
 * the InvoiceDAO table.
 * 
 * @author Théo
 *
 */
@Named("invoiceAction")
@RequestScoped
public class InvoiceDAOImp implements InvoiceDAO, Serializable
{
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	public InvoiceDAOImp() {
		super();
	}

	@Override
	public ArrayList<InvoiceBean> getQueryRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceBean> invoicesList = new ArrayList<InvoiceBean>();
		String preparedQuery = "SELECT * FROM INVOICE";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);
				ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				InvoiceBean invoice = new InvoiceBean();
				invoice.setSaleNumber(rs.getInt("salenumber"));
				invoice.setDate(rs.getTimestamp("dateofsale"));
				invoice.setClientNumber(rs.getString("clientnumber"));
				invoice.setTotalNet(rs.getBigDecimal("totalnet"));
				invoice.setIdpst(rs.getInt("idpst"));
				invoice.setTotalGross(rs.getBigDecimal("totalgross"));
				invoicesList.add(invoice);
			}
		}
		return invoicesList;
	}

	@Override
	public InvoiceBean extractSingleInvoice(int invoiceId) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		InvoiceBean invoice = new InvoiceBean();
		String preparedQuery = "SELECT * FROM INVOICE WHERE salenumber = ? ";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery)) {
			ps.setInt(1, invoiceId);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next())
				{
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setClientNumber(rs.getString("clientnumber"));
					invoice.setTotalNet(rs.getBigDecimal("totalnet"));
					invoice.setIdpst(rs.getInt("idpst"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
				}
			}
		}
		return invoice;
	}

	@Override
	public int insertRecord(InvoiceBean invb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String preparedQuery = "INSERT INTO INVOICE (salenumber, dateofsale, clientnumber, totalnet, idpst, "
				+ "gst, totalgross) VALUES (?,?,?,?,?,?,?)";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);) {

			ps.setInt(1, invb.getSaleNumber());
			ps.setTimestamp(2, invb.getDate());
			ps.setString(3, invb.getClientNumber());
			ps.setBigDecimal(4, invb.getTotalNet());
			ps.setInt(5, invb.getIdpst());
			ps.setBigDecimal(6, invb.getGST());
			ps.setBigDecimal(7, invb.getTotalGross());
			return ps.executeUpdate();
		}
	}

	@Override
	public int updateRecord(InvoiceBean invb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String preparedQuery = "UPDATE INVOICE SET "
				+ "salenumber = ?, dateofsale = ?, clientnumber = ?, totalnet = ?, idpst = ?, "
				+ "gst = ?,totalgross = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery);) {
			ps.setInt(1, invb.getSaleNumber());
			ps.setTimestamp(2, invb.getDate());
			ps.setString(3, invb.getClientNumber());
			ps.setBigDecimal(4, invb.getTotalNet());
			ps.setInt(5, invb.getIdpst());
			ps.setBigDecimal(6, invb.getGST());
			ps.setBigDecimal(7, invb.getTotalGross());
			return ps.executeUpdate();
		}
	}

	@Override
	public int deleteRecord(InvoiceBean ib) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int records = 0;
		String preparedSQL = "DELETE FROM invoice WHERE salenumber = ?";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection.prepareStatement(preparedSQL)) {
			ps.setInt(1, ib.getSaleNumber());
			records = ps.executeUpdate();
		}

		return records;
	}

	@Override
	public ArrayList<InvoiceBean> getInvoiceClient(String clientnb, String date1, String date2) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		System.out.println("invoicedao getinvoice client" + date1 + " et  " + date2 +" clientNB : " + clientnb);
		ArrayList<InvoiceBean> invoicesList = new ArrayList<InvoiceBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
		String preparedQuery = "SELECT DISTINCT(I.salenumber), I.DATEOFSALE, I.TOTALGROSS FROM INVOICE I, CLIENT WHERE I.CLIENTNUMBER = client.id AND CLIENT.EMAIL = ? ORDER BY I.DATEOFSALE";
		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery)) {
			ps.setString(1, clientnb);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					InvoiceBean invoice = new InvoiceBean();
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
					invoicesList.add(invoice);
				}
			}
			return invoicesList;
		}
		}
		else{
			String preparedQuery = "SELECT DISTINCT(I.salenumber), I.DATEOFSALE, I.TOTALGROSS FROM INVOICE I, CLIENT WHERE I.CLIENTNUMBER = client.id AND  CLIENT.EMAIL = ? AND dateofsale between ? AND ? ORDER BY I.DATEOFSALE";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery)) {
				ps.setString(1, clientnb);
				ps.setString(2, date1);
				ps.setString(3, date2);
				try (ResultSet rs = ps.executeQuery()) {
					while (rs.next()) {
						InvoiceBean invoice = new InvoiceBean();
						invoice.setSaleNumber(rs.getInt("salenumber"));
						invoice.setDate(rs.getTimestamp("dateofsale"));
						invoice.setTotalGross(rs.getBigDecimal("totalgross"));
						invoicesList.add(invoice);
					}
				}
				return invoicesList;
			}			
		}
	}

	@Override
	public BigDecimal getTotalGross(String clientnb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		BigDecimal total = null;
		String preparedQuery = "SELECT SUM(DISTINCT(I.TOTALGROSS)) FROM INVOICE I, CLIENT WHERE I.CLIENTNUMBER = client.id AND CLIENT.EMAIL = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery)) {
			ps.setString(1, clientnb);
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {

					total = rs.getBigDecimal(1);
				}
			}
			return total;
		}
	}

	@Override
	public BigDecimal getTotalGrossBookbay() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		BigDecimal total = null;
		String preparedQuery = "SELECT DISTINCT(I.salenumber), SUM(I.TOTALGROSS) FROM INVOICE I";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement ps = connection
						.prepareStatement(preparedQuery)) {
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {

					total = rs.getBigDecimal(2);
				}
			}
			return total;
		}
	}

	@Override
	public ArrayList<InvoiceBean> getTopClients(String date1, String date2) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<InvoiceBean> invoicesList = new ArrayList<InvoiceBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			String preparedQuery = "SELECT I.clientnumber, I.dateofsale, I.salenumber, I.totalnet, I.idpst, sum(I.totalgross) as totalgross FROM INVOICE I GROUP BY I.CLIENTNUMBER ORDER BY TOTALGROSS DESC";

			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					InvoiceBean invoice = new InvoiceBean();
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setClientNumber(rs.getString("clientnumber"));
					invoice.setTotalNet(rs.getBigDecimal("totalnet"));
					invoice.setIdpst(rs.getInt("idpst"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
					invoicesList.add(invoice);
				}
			}
			return invoicesList;
		}
		else{
			String preparedQuery = "SELECT I.clientnumber, I.dateofsale, I.salenumber, I.totalnet, I.idpst, sum(I.totalgross) as totalgross FROM INVOICE I Where dateofsale > ? AND dateofsale < ? GROUP BY I.CLIENTNUMBER ORDER BY TOTALGROSS DESC";

			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, date1);
				ps.setString(2, date2);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceBean invoice = new InvoiceBean();
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setClientNumber(rs.getString("clientnumber"));
					invoice.setTotalNet(rs.getBigDecimal("totalnet"));
					invoice.setIdpst(rs.getInt("idpst"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
					invoicesList.add(invoice);
				}
			}
			return invoicesList;			
		}
	}

	@Override
	public ArrayList<InvoiceBean> getQueryRecordsDate(String date1, String date2)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		System.out.println(date1 + "   et    " + date2);
		ArrayList<InvoiceBean> invoicesList = new ArrayList<InvoiceBean>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			String preparedQuery = "SELECT * FROM INVOICE";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);ResultSet rs = ps.executeQuery()
					) {

				while (rs.next()) {
					InvoiceBean invoice = new InvoiceBean();
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setClientNumber(rs.getString("clientnumber"));
					invoice.setTotalNet(rs.getBigDecimal("totalnet"));
					invoice.setIdpst(rs.getInt("idpst"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
					invoicesList.add(invoice);
				}
			}
			return invoicesList;
		}
		else{
			String preparedQuery = "SELECT * FROM INVOICE WHERE dateofsale > ? AND dateofsale < ?";
			try (Connection connection = dataSource.getConnection();
					PreparedStatement ps = connection
							.prepareStatement(preparedQuery);
					) {
				ps.setString(1, date1);
				ps.setString(2, date2);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					InvoiceBean invoice = new InvoiceBean();
					invoice.setSaleNumber(rs.getInt("salenumber"));
					invoice.setDate(rs.getTimestamp("dateofsale"));
					invoice.setClientNumber(rs.getString("clientnumber"));
					invoice.setTotalNet(rs.getBigDecimal("totalnet"));
					invoice.setIdpst(rs.getInt("idpst"));
					invoice.setTotalGross(rs.getBigDecimal("totalgross"));
					invoicesList.add(invoice);
				}
			}
			return invoicesList;
		}

	}


}
