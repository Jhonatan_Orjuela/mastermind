package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.CustomerReviewBean;

/**
 * This is the interface for the Customer Review Data Access Object.
 * 
 * @author Sean-Frankel Gaon Canlas
 * 
 */
public interface CustomerReviewDAO
{
	/**
	 * This is the method that will retrieve all the records inside the database.
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<CustomerReviewBean> getQueryRecords() throws SQLException;
	
	/**
	 * This is the method that will get specific records from the database.
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<CustomerReviewBean> getSpecificQueryRecords(CustomerReviewBean crb) throws SQLException;
	
	/**
	 * This is the method that will delete a specific record from the database.
	 * @param crb
	 * @return
	 * @throws SQLException
	 */
	public int deleteRecord(CustomerReviewBean crb) throws SQLException;
	
	/**
	 * This is the method that will insert a specific record into the database.
	 * @param crb
	 * @return
	 * @throws SQLException
	 */
	public int insertRecord(CustomerReviewBean crb) throws SQLException;
	
	/**
	 * This is the method that will update a specific record from the database.
	 * @param crb
	 * @return
	 * @throws SQLException
	 */
	public int updateRecord(CustomerReviewBean crb) throws SQLException;
	/**
	 * 
	 * @param crb
	 * @return the review with the key send
	 * @throws SQLException
	 */
	public CustomerReviewBean getSingleReview(int key) throws SQLException;
}
