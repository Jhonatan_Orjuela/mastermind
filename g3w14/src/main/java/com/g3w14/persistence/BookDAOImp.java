package com.g3w14.persistence;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;

import com.g3w14.data.BookBean;

/**
 * Implementation class that enables the ability to manipulate and query records
 * from the Book table.
 * 
 * @author Tyler Patricio, Sophie Leduc Major
 */
@SuppressWarnings("serial")
@Named ("bookAction")
@RequestScoped
public class BookDAOImp implements BookDAO, Serializable {
	@Resource(name = "jdbc/g3w14")
	private DataSource dataSource;

	/**
	 * No-parameter constructor.
	 */
	public BookDAOImp() {
		super();
	}

	/**
	 * Acquires all the records from the book table.
	 * 
	 * @return the ArrayList containing the book data.
	 * @author Tyler Patricio
	 */
	@Override
	public ArrayList<BookBean> getAllRecords() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		ArrayList<BookBean> rows = new ArrayList<>();
		String sql = "SELECT * FROM book ORDER BY TITLE";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				BookBean bb = new BookBean();

				bb.setIsbn10(resultSet.getString("isbn10"));
				bb.setIsbn13(resultSet.getString("isbn13"));
				bb.setTitle(resultSet.getString("title"));
				bb.setAuthor(resultSet.getString("author"));
				bb.setPublisher(resultSet.getString("publisher"));
				bb.setNumOfPages(resultSet.getInt("numberofpages"));
				bb.setGenre(resultSet.getString("genre"));
				bb.setBookCoverImg(resultSet.getString("image"));
				bb.setBookType(resultSet.getInt("booktype"));
				bb.setEbookFormat(resultSet.getString("ebookformats"));
				bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
				bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
				bb.setListPrice(resultSet.getBigDecimal("listprice"));
				bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
				bb.setWeight(resultSet.getDouble("weight"));
				bb.setDimensions(resultSet.getString("dimensions"));
				bb.setDateEntered(resultSet.getTimestamp("dateentered"));
				bb.setRemovalStatus(resultSet.getInt("removalstatus"));

				rows.add(bb);
			}
		}

		return rows;
	}

	/**
	 * getQuerySingleBook
	 * 
	 * This method returns a single book found with its unique book isbn13
	 * @author Théo
	 * @return bookBean row: client bean with the matching isbn13.
	 * @throws SQLException
	 * 
	 */
	@Override
	public BookBean getQuerySingleBook(String isbn13)
			throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		String sql = "SELECT * FROM book WHERE isbn13 = ?";
		ArrayList<BookBean> rows = new ArrayList<BookBean>();
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, isbn13);

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {

					BookBean bb = new BookBean();

					bb.setIsbn10(resultSet.getString("isbn10"));
					bb.setIsbn13(resultSet.getString("isbn13"));
					bb.setTitle(resultSet.getString("title"));
					bb.setAuthor(resultSet.getString("author"));
					bb.setPublisher(resultSet.getString("publisher"));
					bb.setNumOfPages(resultSet.getInt("numberofpages"));
					bb.setGenre(resultSet.getString("genre"));
					bb.setBookCoverImg(resultSet.getString("image"));
					bb.setBookType(resultSet.getInt("booktype"));
					bb.setEbookFormat(resultSet.getString("ebookformats"));
					bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
					bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
					bb.setListPrice(resultSet.getBigDecimal("listprice"));
					bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
					bb.setWeight(resultSet.getDouble("weight"));
					bb.setDimensions(resultSet.getString("dimensions"));
					bb.setDateEntered(resultSet.getTimestamp("dateentered"));
					bb.setRemovalStatus(resultSet.getInt("removalstatus"));
					rows.add(bb);
				}
			}
		}

		if(rows.size()!=1)
			rows.add(null);

		return rows.get(0);
	}
	/**
	 * Acquires the desired records based on a query String.
	 * 
	 * @param bb
	 *            data used to generate the SQL statement.
	 * @return the ArrayList containing the book data based on the query.
	 * @author Tyler Patricio
	 */
	@Override
	public ArrayList<BookBean> advancedSearch(BookBean bb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<BookBean> rows = new ArrayList<>();
		ArrayList<Object> queryInfo = new ArrayList<>();

		queryInfo = sqlMaker(bb);

		// variable used to hold size of ArrayList for efficiency of looping
		// condition
		int size = queryInfo.size() - 1;

		String sql = (String) queryInfo.get(size);

		System.out.println("this is what the query is :" + sql);

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			for (int i = 0; i < size; i++) {
				// check fields for specific class to properly set values in
				// prepared statement

				if (queryInfo.get(i) instanceof String)
					pStatement.setString(i + 1, (String) queryInfo.get(i));

				else if (queryInfo.get(i) instanceof BigDecimal)
					pStatement.setBigDecimal(i + 1,
							(BigDecimal) queryInfo.get(i));

				else if (queryInfo.get(i) instanceof Integer)
					pStatement.setInt(i + 1, (int) queryInfo.get(i));

				else if (queryInfo.get(i) instanceof Double)
					pStatement.setDouble(i + 1, (double) queryInfo.get(i));

				else if (queryInfo.get(i) instanceof Timestamp)
					pStatement
					.setTimestamp(i + 1, (Timestamp) queryInfo.get(i));
			}

			try (ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					BookBean bookBean = new BookBean();

					bookBean.setIsbn10(resultSet.getString("isbn10"));
					bookBean.setIsbn13(resultSet.getString("isbn13"));
					bookBean.setTitle(resultSet.getString("title"));
					bookBean.setAuthor(resultSet.getString("author"));
					bookBean.setPublisher(resultSet.getString("publisher"));
					bookBean.setNumOfPages(resultSet.getInt("numberofpages"));
					bookBean.setGenre(resultSet.getString("genre"));
					bookBean.setBookCoverImg(resultSet.getString("image"));
					bookBean.setBookType(resultSet.getInt("booktype"));
					bookBean.setEbookFormat(resultSet.getString("ebookformats"));
					bookBean.setNumOfCopies(resultSet.getInt("numberofcopies"));
					bookBean.setWholeSalePrice(resultSet
							.getBigDecimal("wholesaleprice"));
					bookBean.setListPrice(resultSet.getBigDecimal("listprice"));
					bookBean.setSalePrice(resultSet.getBigDecimal("saleprice"));
					bookBean.setWeight(resultSet.getDouble("weight"));
					bookBean.setDimensions(resultSet.getString("dimensions"));
					bookBean.setDateEntered(resultSet
							.getTimestamp("dateentered"));
					bookBean.setRemovalStatus(resultSet.getInt("removalstatus"));

					rows.add(bookBean);
				}
			} // end inner-try
		}

		return rows;
	}

	/**
	 * Deletes a specified record from the book table.
	 * 
	 * @return the counter value for the newly deleted record (either 0 or 1)
	 * @author Tyler Patricio
	 */
	@Override
	public int deleteRecord(String isbn13) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		// record counter for newly deleted row
		int record;

		String sql = "DELETE FROM book WHERE isbn13 = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);) {
			pStatement.setString(1, isbn13);
			record = pStatement.executeUpdate();
		}

		return record;
	}

	/**
	 * Inserts a specified record into the book table.
	 * 
	 * @return the counter value for the newly inserted record (either 0 or 1)
	 * @author Tyler Patricio
	 */
	@Override
	public int insertRecord(BookBean bb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		// record counter for newly added row
		int record;

		String preparedQuery = "INSERT INTO book (isbn10, isbn13, title, author, publisher, numberofpages,"
				+ " genre, image, booktype, ebookformats, numberofcopies, wholesaleprice, listprice, saleprice,"
				+ " weight, dimensions, dateentered, removalstatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedQuery);) {
			pStatement.setString(1, bb.getIsbn10());
			pStatement.setString(2, bb.getIsbn13());
			pStatement.setString(3, bb.getTitle());
			pStatement.setString(4, bb.getAuthor());
			pStatement.setString(5, bb.getPublisher());
			pStatement.setInt(6, bb.getNumOfPages());
			pStatement.setString(7, bb.getGenre());
			pStatement.setString(8, bb.getBookCoverImg());
			pStatement.setInt(9, bb.getBookType());
			pStatement.setString(10, bb.getEbookFormat());
			pStatement.setInt(11, bb.getNumOfCopies());
			pStatement.setBigDecimal(12, bb.getWholeSalePrice());
			pStatement.setBigDecimal(13, bb.getListPrice());
			pStatement.setBigDecimal(14, bb.getSalePrice());
			pStatement.setDouble(15, bb.getWeight());
			pStatement.setString(16, bb.getDimensions());
			pStatement.setTimestamp(17, new Timestamp((new java.util.Date()).getTime()));
			pStatement.setInt(18, bb.getRemovalStatus());

			record = pStatement.executeUpdate();
		}

		return record;
	}

	/**
	 * Updates specified records from the book table.
	 * 
	 * @return the counter value for the newly updated record (either 0 or 1)
	 * @author Tyler Patricio
	 */
	@Override
	public int updateRecord(BookBean bb) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		int record;
		String preparedSQL = "UPDATE book SET isbn10 = ?, title = ?, author = ?,"
				+ "publisher = ?, numberofpages = ?, genre = ?, image = ?, booktype = ?, ebookformats = ?, "
				+ "numberofcopies = ?, wholesaleprice = ?, listprice = ?, saleprice = ?, weight = ?, "
				+ "dimensions = ?, dateentered = ?, removalstatus = ? WHERE isbn13 = ?";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection
						.prepareStatement(preparedSQL);) {
			pStatement.setString(1, bb.getIsbn10());
			pStatement.setString(2, bb.getTitle());
			pStatement.setString(3, bb.getAuthor());
			pStatement.setString(4, bb.getPublisher());
			pStatement.setInt(5, bb.getNumOfPages());
			pStatement.setString(6, bb.getGenre());
			pStatement.setString(7, bb.getBookCoverImg());
			pStatement.setInt(8, bb.getBookType());
			pStatement.setString(9, bb.getEbookFormat());
			pStatement.setInt(10, bb.getNumOfCopies());
			pStatement.setBigDecimal(11, bb.getWholeSalePrice());
			pStatement.setBigDecimal(12, bb.getListPrice());
			pStatement.setBigDecimal(13, bb.getSalePrice());
			pStatement.setDouble(14, bb.getWeight());
			pStatement.setString(15, bb.getDimensions());
			pStatement.setTimestamp(16, bb.getDateEntered());
			pStatement.setInt(17, bb.getRemovalStatus());
			pStatement.setString(18, bb.getIsbn13());
			System.out.println("AVANT UPDATE");
			record = pStatement.executeUpdate();
			System.out.println("APRES UPDATE");
		}

		return record;
	}

	/**
	 * Generates the SQL statement for searching by a specific query.
	 * 
	 * @param bb
	 *            data used to generate the SQL statement
	 * @return the list containing not null field values along with the newly
	 *         constructed SQL statement
	 *         
	 * @author Tyler Patricio
	 */
	private ArrayList<Object> sqlMaker(BookBean bb) {
		ArrayList<Object> queryInfo = new ArrayList<>();

		String sql = "SELECT * FROM book WHERE";

		if (bb.getIsbn10().length() != 0) {
			queryInfo.add(bb.getIsbn10());
			sql += " isbn10 = ? AND";
		}

		if (bb.getIsbn13().length() != 0) {
			queryInfo.add(bb.getIsbn13());
			sql += " isbn13 = ? AND";
		}

		if (bb.getTitle().length() != 0) {
			queryInfo.add(bb.getTitle());
			sql += " title LIKE ? AND";
		}

		if (bb.getAuthor().length() != 0) {
			queryInfo.add(bb.getAuthor());
			sql += " author LIKE ? AND";
		}

		if (bb.getPublisher().length() != 0) {
			queryInfo.add(bb.getPublisher());
			sql += " publisher = ? AND";
		}

		if (bb.getNumOfPages() != 0) {
			queryInfo.add(bb.getNumOfPages());
			sql += " numberofpages = ? AND";
		}

		if (bb.getGenre().length() != 0) {
			queryInfo.add(bb.getGenre());
			sql += " genre = ? AND";
		}

		if (bb.getBookCoverImg().length() != 0) {
			queryInfo.add(bb.getBookCoverImg());
			sql += " image = ? AND";
		}

		if (bb.getBookType() != 0) {
			queryInfo.add(bb.getBookType());
			sql += " booktype = ? AND";
		}

		if (bb.getEbookFormat().length() != 0) {
			queryInfo.add(bb.getEbookFormat());
			sql += " ebookformats = ? AND";
		}

		if (bb.getNumOfCopies() != 0) {
			queryInfo.add(bb.getNumOfCopies());
			sql += " numberofcopies = ? AND";
		}

		if (bb.getWholeSalePrice() != null
				&& bb.getWholeSalePrice().compareTo(new BigDecimal(-1)) > 0) {
			queryInfo.add(bb.getWholeSalePrice());
			sql += " wholesaleprice = ? AND";
		}

		if (bb.getListPrice() != null
				&& bb.getListPrice().compareTo(new BigDecimal(-1)) > 0) {
			queryInfo.add(bb.getListPrice());
			sql += " listprice = ? AND";
		}

		if (bb.getSalePrice() != null
				&& bb.getSalePrice().compareTo(new BigDecimal(-1)) > 0) {
			queryInfo.add(bb.getSalePrice());
			sql += " saleprice = ? AND";
		}

		if (bb.getWeight() != 0.0) {
			queryInfo.add(bb.getWeight());
			sql += " weight = ? AND";
		}

		if (bb.getDimensions().length() != 0) {
			queryInfo.add(bb.getDimensions());
			sql += " dimensions = ? AND";
		}

		if (bb.getDateEntered() != null && bb.getDateEntered().getTime() != 0) {
			queryInfo.add(bb.getDateEntered());
			sql += " dateentered = ? AND";
		}

		if (bb.getRemovalStatus() != 0) {
			queryInfo.add(bb.getRemovalStatus());
			sql += " removalstatus = ? AND";
		}

		sql = sql.substring(0, sql.length() - 4);
		queryInfo.add(sql);

		return queryInfo;
	}

	/**
	 * This gets our best sellers according to how many we have sold (through the 
	 * invoice details table.) It limits to 10 books to display in our banner.
	 * 
	 * @return
	 * @throws SQLException
	 * @author Sophie Leduc Major
	 */
	@Override
	public ArrayList<BookBean> retriveBestSellers() throws SQLException {

		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<BookBean> rows = new ArrayList<BookBean>();

		//MEGA SELECT STATEMENT OF DOOOOOOOM... this gets the 10 best sellers.
		String sql = "SELECT book.isbn10, book.isbn13, book.title, book.author, book.publisher,"
				+ "book.numberofpages, book.genre, book.image, book.booktype, book.ebookformats,"
				+ "book.numberofcopies, book.wholesaleprice, book.listprice, book.saleprice, "
				+ "book.weight, book.dimensions, book.dateentered, book.Removalstatus ,"
				+ "count(invoicedetail.bookisbn) c FROM book, invoicedetail WHERE "
				+ "book.isbn13 = invoicedetail.bookisbn GROUP BY invoicedetail.bookisbn "
				+ "ORDER BY c DESC limit 10;";

		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				BookBean bb = new BookBean();

				bb.setIsbn10(resultSet.getString("isbn10"));
				bb.setIsbn13(resultSet.getString("isbn13"));
				bb.setTitle(resultSet.getString("title"));
				bb.setAuthor(resultSet.getString("author"));
				bb.setPublisher(resultSet.getString("publisher"));
				bb.setNumOfPages(resultSet.getInt("numberofpages"));
				bb.setGenre(resultSet.getString("genre"));
				bb.setBookCoverImg(resultSet.getString("image"));
				bb.setBookType(resultSet.getInt("booktype"));
				bb.setEbookFormat(resultSet.getString("ebookformats"));
				bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
				bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
				bb.setListPrice(resultSet.getBigDecimal("listprice"));
				bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
				bb.setWeight(resultSet.getDouble("weight"));
				bb.setDimensions(resultSet.getString("dimensions"));
				bb.setDateEntered(resultSet.getTimestamp("dateentered"));
				bb.setRemovalStatus(resultSet.getInt("removalstatus"));

				rows.add(bb);
			}
		}

		return rows;

	}

	@Override
	public ArrayList<String> getAllAuthor() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<String> rows = new ArrayList<>();
		String sql = "SELECT DISTINCT(author) FROM book ORDER BY AUTHOR";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				rows.add(resultSet.getString("author"));
			}
		}

		return rows;
	}

	@Override
	public ArrayList<String> getAllPublisher() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<String> rows = new ArrayList<>();
		String sql = "SELECT DISTINCT(publisher) FROM book ORDER BY PUBLISHER";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				rows.add(resultSet.getString("publisher"));
			}
		}

		return rows;
	}

	@Override
	public ArrayList<BookBean> getZeroSales(String date1, String date2) throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<BookBean> rows = new ArrayList<>();
		if(date1 == null | date1 == "" | date2 == null | date2 == ""){
			String sql = "SELECT * FROM book b WHERE"
					+ " b.isbn13 NOT IN(SELECT bo.isbn13 from book bo, invoicedetail i where bo.isbn13=i.bookisbn) GROUP BY B.ISBN13 ORDER BY TITLE";

			// try with resources block
			try (Connection connection = dataSource.getConnection();
					PreparedStatement pStatement = connection.prepareStatement(sql);
					ResultSet resultSet = pStatement.executeQuery();) {
				while (resultSet.next()) {
					BookBean bb = new BookBean();

					bb.setIsbn10(resultSet.getString("isbn10"));
					bb.setIsbn13(resultSet.getString("isbn13"));
					bb.setTitle(resultSet.getString("title"));
					bb.setAuthor(resultSet.getString("author"));
					bb.setPublisher(resultSet.getString("publisher"));
					bb.setNumOfPages(resultSet.getInt("numberofpages"));
					bb.setGenre(resultSet.getString("genre"));
					bb.setBookCoverImg(resultSet.getString("image"));
					bb.setBookType(resultSet.getInt("booktype"));
					bb.setEbookFormat(resultSet.getString("ebookformats"));
					bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
					bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
					bb.setListPrice(resultSet.getBigDecimal("listprice"));
					bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
					bb.setWeight(resultSet.getDouble("weight"));
					bb.setDimensions(resultSet.getString("dimensions"));
					bb.setDateEntered(resultSet.getTimestamp("dateentered"));
					bb.setRemovalStatus(resultSet.getInt("removalstatus"));

					rows.add(bb);
				}
			}

			return rows;
		}
		else{
			String sql = "SELECT * FROM book b WHERE"
					+ " b.isbn13 NOT IN(SELECT bo.isbn13 from book bo, invoicedetail i where bo.isbn13=i.bookisbn and i.salenumber in (select salenumber from invoice where dateofsale > ? and dateofsale < ?)) GROUP BY B.ISBN13 ORDER BY TITLE";

			// try with resources block
			try (Connection connection = dataSource.getConnection();
					PreparedStatement pStatement = connection.prepareStatement(sql);
					) {
				pStatement.setString(1, date1);
				pStatement.setString(2, date2);
				ResultSet resultSet = pStatement.executeQuery();
				while (resultSet.next()) {
					BookBean bb = new BookBean();
					bb.setIsbn10(resultSet.getString("isbn10"));
					bb.setIsbn13(resultSet.getString("isbn13"));
					bb.setTitle(resultSet.getString("title"));
					bb.setAuthor(resultSet.getString("author"));
					bb.setPublisher(resultSet.getString("publisher"));
					bb.setNumOfPages(resultSet.getInt("numberofpages"));
					bb.setGenre(resultSet.getString("genre"));
					bb.setBookCoverImg(resultSet.getString("image"));
					bb.setBookType(resultSet.getInt("booktype"));
					bb.setEbookFormat(resultSet.getString("ebookformats"));
					bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
					bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
					bb.setListPrice(resultSet.getBigDecimal("listprice"));
					bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
					bb.setWeight(resultSet.getDouble("weight"));
					bb.setDimensions(resultSet.getString("dimensions"));
					bb.setDateEntered(resultSet.getTimestamp("dateentered"));
					bb.setRemovalStatus(resultSet.getInt("removalstatus"));

					rows.add(bb);
				}
			}
			return rows;			
		}
	}

	@Override
	public ArrayList<BookBean> getReorder() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<BookBean> rows = new ArrayList<>();
		String sql = "SELECT * FROM book WHERE  numberofcopies < 5 ORDER BY TITLE";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				BookBean bb = new BookBean();

				bb.setIsbn10(resultSet.getString("isbn10"));
				bb.setIsbn13(resultSet.getString("isbn13"));
				bb.setTitle(resultSet.getString("title"));
				bb.setAuthor(resultSet.getString("author"));
				bb.setPublisher(resultSet.getString("publisher"));
				bb.setNumOfPages(resultSet.getInt("numberofpages"));
				bb.setGenre(resultSet.getString("genre"));
				bb.setBookCoverImg(resultSet.getString("image"));
				bb.setBookType(resultSet.getInt("booktype"));
				bb.setEbookFormat(resultSet.getString("ebookformats"));
				bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
				bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
				bb.setListPrice(resultSet.getBigDecimal("listprice"));
				bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
				bb.setWeight(resultSet.getDouble("weight"));
				bb.setDimensions(resultSet.getString("dimensions"));
				bb.setDateEntered(resultSet.getTimestamp("dateentered"));
				bb.setRemovalStatus(resultSet.getInt("removalstatus"));

				rows.add(bb);
			}
		}

		return rows;
	}

	/**
	 * Returns all of the books on sale for the index page carousel.
	 * 
	 * @author Tyler Patricio
	 * @return
	 * @throws SQLException
	 */
	@Override
	public ArrayList<BookBean> getBooksOnSale() throws SQLException 
	{
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");

		ArrayList<BookBean> rows = new ArrayList<>();
		String sql = "SELECT * FROM book WHERE saleprice != 0.00";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				BookBean bb = new BookBean();

				bb.setIsbn10(resultSet.getString("isbn10"));
				bb.setIsbn13(resultSet.getString("isbn13"));
				bb.setTitle(resultSet.getString("title"));
				bb.setAuthor(resultSet.getString("author"));
				bb.setPublisher(resultSet.getString("publisher"));
				bb.setNumOfPages(resultSet.getInt("numberofpages"));
				bb.setGenre(resultSet.getString("genre"));
				bb.setBookCoverImg(resultSet.getString("image"));
				bb.setBookType(resultSet.getInt("booktype"));
				bb.setEbookFormat(resultSet.getString("ebookformats"));
				bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
				bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
				bb.setListPrice(resultSet.getBigDecimal("listprice"));
				bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
				bb.setWeight(resultSet.getDouble("weight"));
				bb.setDimensions(resultSet.getString("dimensions"));
				bb.setDateEntered(resultSet.getTimestamp("dateentered"));
				bb.setRemovalStatus(resultSet.getInt("removalstatus"));

				rows.add(bb);
			}

			return rows;
		}
	}

	@Override
	public ArrayList<BookBean> getTopSellers() throws SQLException {
		if (dataSource == null)
			throw new SQLException("ERROR. Can't get the data source.");
		ArrayList<BookBean> rows = new ArrayList<>();
		String sql = "SELECT * FROM book WHERE isbn13 in (SELECT DISTINCT(I.BOOKISBN) FROM INVOICEDETAIL I GROUP BY I.BOOKISBN ORDER BY QUANTITY DESC)";

		// try with resources block
		try (Connection connection = dataSource.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql);
				ResultSet resultSet = pStatement.executeQuery();) {
			while (resultSet.next()) {
				BookBean bb = new BookBean();

				bb.setIsbn10(resultSet.getString("isbn10"));
				bb.setIsbn13(resultSet.getString("isbn13"));
				bb.setTitle(resultSet.getString("title"));
				bb.setAuthor(resultSet.getString("author"));
				bb.setPublisher(resultSet.getString("publisher"));
				bb.setNumOfPages(resultSet.getInt("numberofpages"));
				bb.setGenre(resultSet.getString("genre"));
				bb.setBookCoverImg(resultSet.getString("image"));
				bb.setBookType(resultSet.getInt("booktype"));
				bb.setEbookFormat(resultSet.getString("ebookformats"));
				bb.setNumOfCopies(resultSet.getInt("numberofcopies"));
				bb.setWholeSalePrice(resultSet.getBigDecimal("wholesaleprice"));
				bb.setListPrice(resultSet.getBigDecimal("listprice"));
				bb.setSalePrice(resultSet.getBigDecimal("saleprice"));
				bb.setWeight(resultSet.getDouble("weight"));
				bb.setDimensions(resultSet.getString("dimensions"));
				bb.setDateEntered(resultSet.getTimestamp("dateentered"));
				bb.setRemovalStatus(resultSet.getInt("removalstatus"));

				rows.add(bb);
			}
		}

		return rows;
	}
} 