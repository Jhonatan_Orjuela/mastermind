package com.g3w14.persistence;

import java.sql.SQLException;
import java.util.ArrayList;

import com.g3w14.data.AdvertisementBean;

/**
 * This is the DAO interface for the Advertisement bean/database table.
 * 
 * @author Sean-Frankel Gaon Canlas
 * @author Sophie Leduc Major
 *
 */
public interface AdvertisementDAO {
	
	/**
	 * This method gets all the query records in the database.
	 * @return ArrayList of all the query records in an advertisement bean
	 * @throws SQLException
	 */
	public ArrayList<AdvertisementBean> getQueryRecords() throws SQLException;
	
	/**
	 * This method gets specific records from the database.
	 * @return ArrayList of the specific query records
	 * @throws SQLException
	 */
	public ArrayList<AdvertisementBean> getSpecificQueryRecords(String adFileName) throws SQLException;

	/**
	 * This method deletes a specific record in the table.
	 * @param ab the bean to delete
	 * @return
	 * @throws SQLException
	 */
	public int deleteRecord(AdvertisementBean ab) throws SQLException;
	
	/**
	 * This method inserts a record into the database.
	 * @param ab
	 * @return
	 * @throws SQLException
	 */
	public int insertRecord(AdvertisementBean ab) throws SQLException;
	
	/**
	 * This method updates a record in the database.
	 * @param ab
	 * @return
	 * @throws SQLException
	 */
	public int updateRecord(AdvertisementBean ab) throws SQLException;
}
