/**
 * 
 */
package com.g3w14.data;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 * @author Jhonatan Orjuela
 * 
 **/
@Named("rssBean")
@SessionScoped
public class RSSBean implements Serializable {

	private static final long serialVersionUID = 8740022670799314589L;
	private boolean display;
	private String rssUrl;
	private int id;

	/**
	 * 
	 */
	public RSSBean() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (display ? 1231 : 1237);
		result = prime * result + id;
		result = prime * result + ((rssUrl == null) ? 0 : rssUrl.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RSSBean other = (RSSBean) obj;
		if (display != other.display)
			return false;
		if (id != other.id)
			return false;
		if (rssUrl == null) {
			if (other.rssUrl != null)
				return false;
		} else if (!rssUrl.equals(other.rssUrl))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RSSBean [display=" + display + ", rssUrl=" + rssUrl + ", id="
				+ id + "]";
	}

	/**
	 * @return the display
	 */
	public boolean isDisplay() {
		return display;
	}

	/**
	 * @param display
	 *            the display to set
	 */
	public void setDisplay(boolean display) {
		this.display = display;
	}

	/**
	 * @return the rssUrl
	 */
	public String getRssUrl() {
		return rssUrl;
	}

	/**
	 * @param rssUrl
	 *            the rssUrl to set
	 */
	public void setRssUrl(String rssUrl) {
		this.rssUrl = rssUrl;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}
