package com.g3w14.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the Book table.
 * 
 * @author Tyler Patricio
 */
@SuppressWarnings("serial")
@Named("bookBean")
@ViewScoped
public class BookBean implements Serializable
{
	private String isbn10;
	private String isbn13;
	private String title;
	private String author;
	private String publisher;
	private int numOfPages;
	private String genre;
	private String bookCoverImg;
	private int bookType;
	private String ebookFormat;
	private int numOfCopies;
	private BigDecimal wholeSalePrice;
	private BigDecimal listPrice;
	private BigDecimal salePrice;
	private double weight;
	private String dimensions;
	private Timestamp dateEntered;
	private int removalStatus;
	private int quantity = 0;
	private BigDecimal bookPricing = new BigDecimal("0.00");
	private String bookFormat = "";

	/**
	 * Default no-parameter constructor.
	 */
	public BookBean ()
	{
		this.isbn10 = "";
		this.isbn13 = "";
		this.title = "";
		this.author = "";
		this.publisher = "";
		this.numOfPages = 0;
		this.genre = "";
		this.bookCoverImg = "";
		this.bookType = 0;
		this.ebookFormat = "";
		this.numOfCopies = 0;
		this.wholeSalePrice = new BigDecimal(-1.00);
		this.listPrice = new BigDecimal(-1.00);
		this.salePrice = new BigDecimal(-1.00);
		this.weight = 0.0;
		this.dimensions = "";
		this.dateEntered = null;
		this.removalStatus = 0;
	}

	/**
	 * Parameter-filled constructor.
	 * 
	 * @param isbn10
	 * @param isbn13
	 * @param title
	 * @param author
	 * @param publisher
	 * @param numOfPages
	 * @param genre
	 * @param bookCoverImg
	 * @param bookType
	 * @param ebookFormat
	 * @param numOfCopies
	 * @param wholeSalePrice
	 * @param listPrice
	 * @param salePrice
	 * @param weight
	 * @param dimensions
	 * @param dateEntered
	 * @param removalStatus
	 */
	public BookBean(String isbn10, String isbn13, String title, String author, String publisher,
			int numOfPages, String genre, String bookCoverImg, int bookType,
			String ebookFormat, int numOfCopies, BigDecimal wholeSalePrice,
			BigDecimal listPrice, BigDecimal salePrice, double weight,
			String dimensions, Timestamp dateEntered, int removalStatus)
	{
		this.isbn10 = isbn10;
		this.isbn13 = isbn13;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.numOfPages = numOfPages;
		this.genre = genre;
		this.bookCoverImg = bookCoverImg;
		this.bookType = bookType;
		this.ebookFormat = ebookFormat;
		this.numOfCopies = numOfCopies;
		this.wholeSalePrice = wholeSalePrice;
		this.listPrice = listPrice;
		this.salePrice = salePrice;
		this.weight = weight;
		this.dimensions = dimensions;
		this.dateEntered = dateEntered;
		this.removalStatus = removalStatus;
	}

	/**
	 * @return the isbn10
	 */
	public String getIsbn10()
	{
		return isbn10;
	}

	/**
	 * @param isbn10
	 *            the isbn10 to set
	 */
	public void setIsbn10(String isbn10)
	{
		this.isbn10 = isbn10;
	}

	/**
	 * @return the isbn13
	 */
	public String getIsbn13()
	{
		return isbn13;
	}

	/**
	 * @param isbn13 the isbn13 to set
	 */
	public void setIsbn13(String isbn13)
	{
		this.isbn13 = isbn13;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the author
	 */
	public String getAuthor()
	{
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author)
	{
		this.author = author;
	}

	/**
	 * @return the publisher
	 */
	public String getPublisher()
	{
		return publisher;
	}

	/**
	 * @param publisher
	 *            the publisher to set
	 */
	public void setPublisher(String publisher)
	{
		this.publisher = publisher;
	}

	/**
	 * @return the numOfPages
	 */
	public int getNumOfPages()
	{
		return numOfPages;
	}

	/**
	 * @param numOfPages
	 *            the numOfPages to set
	 */
	public void setNumOfPages(int numOfPages)
	{
		this.numOfPages = numOfPages;
	}

	/**
	 * @return the genre
	 */
	public String getGenre()
	{
		return genre;
	}

	/**
	 * @param genre
	 *            the genre to set
	 */
	public void setGenre(String genre)
	{
		this.genre = genre;
	}

	/**
	 * @return the bookCoverImg
	 */
	public String getBookCoverImg()
	{
		return bookCoverImg;
	}

	/**
	 * @param bookCoverImg
	 *            the bookCoverImg to set
	 */
	public void setBookCoverImg(String bookCoverImg)
	{
		this.bookCoverImg = bookCoverImg;
	}

	/**
	 * @return the bookType
	 */
	public int getBookType()
	{
		return bookType;
	}

	/**
	 * @param bookType
	 *            the bookType to set
	 */
	public void setBookType(int bookType)
	{
		this.bookType = bookType;
	}

	/**
	 * @return the ebookFormat
	 */
	public String getEbookFormat()
	{
		return ebookFormat;
	}

	/**
	 * @param ebookFormat
	 *            the ebookFormat to set
	 */
	public void setEbookFormat(String ebookFormat)
	{
		this.ebookFormat = ebookFormat;
	}

	/**
	 * @return the numOfCopies
	 */
	public int getNumOfCopies()
	{
		return numOfCopies;
	}

	/**
	 * @param numOfCopies
	 *            the numOfCopies to set
	 */
	public void setNumOfCopies(int numOfCopies)
	{
		this.numOfCopies = numOfCopies;
	}

	/**
	 * @return the wholeSalePrice
	 */
	public BigDecimal getWholeSalePrice()
	{
		return wholeSalePrice;
	}

	/**
	 * @param wholeSalePrice
	 *            the wholeSalePrice to set
	 */
	public void setWholeSalePrice(BigDecimal wholeSalePrice)
	{
		this.wholeSalePrice = wholeSalePrice;
	}

	/**
	 * @return the listPrice
	 */
	public BigDecimal getListPrice()
	{
		return listPrice;
	}

	/**
	 * @param listPrice
	 *            the listPrice to set
	 */
	public void setListPrice(BigDecimal listPrice)
	{
		this.listPrice = listPrice;
	}

	/**
	 * @return the salePrice
	 */
	public BigDecimal getSalePrice()
	{
		return salePrice;
	}

	/**
	 * @param salePrice
	 *            the salePrice to set
	 */
	public void setSalePrice(BigDecimal salePrice)
	{
		this.salePrice = salePrice;
	}

	/**
	 * @return the weight
	 */
	public double getWeight()
	{
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(double weight)
	{
		this.weight = weight;
	}

	/**
	 * @return the dimensions
	 */
	public String getDimensions()
	{
		return dimensions;
	}

	/**
	 * @param dimensions
	 *            the dimensions to set
	 */
	public void setDimensions(String dimensions)
	{
		this.dimensions = dimensions;
	}

	/**
	 * @return the dateEntered
	 */
	public Timestamp getDateEntered()
	{
//		Timestamp currentDate = new Timestamp((new java.util.Date()).getTime());
		return dateEntered;
	}

	/**
	 * @param dateEntered
	 *            the dateEntered to set
	 */
	public void setDateEntered(Timestamp dateEntered)
	{
		this.dateEntered = dateEntered;
	}

	/**
	 * @return the removalStatus
	 */
	public int getRemovalStatus()
	{
		return removalStatus;
	}
	
	/**
	 * @param removalStatus
	 *            the removalStatus to set
	 */
	public void setRemovalStatus(int removalStatus)
	{
		this.removalStatus = removalStatus;
	}
	
	/**
	 * @return the quantity
	 */
	public int getQuantity()
	{
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) 
	{
		this.quantity = quantity;
	}
	
	/**
	 * @return the bookPricing
	 */
	public BigDecimal getBookPricing()
	{
		return bookPricing;
	}

	/**
	 * @param bookPricing the bookPricing to set
	 */
	public void setBookPricing(BigDecimal bookPricing) 
	{
		this.bookPricing = bookPricing;
	}
	
	/**
	 * @return the bookFormat
	 */
	public String getBookFormat()
	{
		if (bookType == 1)
			return "Trade Paperback";
		
		else if (bookType == 2)
			return "Hardcover";
		
		else
			return "Ebook";
	}

	/**
	 * @param bookFormat the bookFormat to set
	 */
	public void setBookFormat(String bookFormat)
	{
		this.bookFormat = bookFormat;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "BookBean [isbn10=" + isbn10 + ", isbn13=" + isbn13 + ", title="
				+ title + ", author=" + author + ", publisher=" + publisher
				+ ", numOfPages=" + numOfPages + ", genre=" + genre
				+ ", bookCoverImg=" + bookCoverImg + ", bookType=" + bookType
				+ ", ebookFormat=" + ebookFormat + ", numOfCopies="
				+ numOfCopies + ", wholeSalePrice=" + wholeSalePrice
				+ ", listPrice=" + listPrice + ", salePrice=" + salePrice
				+ ", weight=" + weight + ", dimensions=" + dimensions
				+ ", dateEntered=" + dateEntered + ", removalStatus="
				+ removalStatus + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((bookCoverImg == null) ? 0 : bookCoverImg.hashCode());
		result = prime * result + bookType;
		result = prime * result
				+ ((dateEntered == null) ? 0 : dateEntered.hashCode());
		result = prime * result
				+ ((dimensions == null) ? 0 : dimensions.hashCode());
		result = prime * result
				+ ((ebookFormat == null) ? 0 : ebookFormat.hashCode());
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((isbn10 == null) ? 0 : isbn10.hashCode());
		result = prime * result + ((isbn13 == null) ? 0 : isbn13.hashCode());
		result = prime * result
				+ ((listPrice == null) ? 0 : listPrice.hashCode());
		result = prime * result + numOfCopies;
		result = prime * result + numOfPages;
		result = prime * result
				+ ((publisher == null) ? 0 : publisher.hashCode());
		result = prime * result + removalStatus;
		result = prime * result
				+ ((salePrice == null) ? 0 : salePrice.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((wholeSalePrice == null) ? 0 : wholeSalePrice.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BookBean other = (BookBean) obj;
		if (author == null)
		{
			if (other.author != null)
				return false;
		}
		else if (!author.equals(other.author))
			return false;
		if (bookCoverImg == null)
		{
			if (other.bookCoverImg != null)
				return false;
		}
		else if (!bookCoverImg.equals(other.bookCoverImg))
			return false;
		if (bookType != other.bookType)
			return false;
		if (dateEntered == null)
		{
			if (other.dateEntered != null)
				return false;
		}
		else if (!dateEntered.equals(other.dateEntered))
			return false;
		if (dimensions == null)
		{
			if (other.dimensions != null)
				return false;
		}
		else if (!dimensions.equals(other.dimensions))
			return false;
		if (ebookFormat == null)
		{
			if (other.ebookFormat != null)
				return false;
		}
		else if (!ebookFormat.equals(other.ebookFormat))
			return false;
		if (genre == null)
		{
			if (other.genre != null)
				return false;
		}
		else if (!genre.equals(other.genre))
			return false;
		if (isbn10 == null)
		{
			if (other.isbn10 != null)
				return false;
		}
		else if (!isbn10.equals(other.isbn10))
			return false;
		if (isbn13 == null)
		{
			if (other.isbn13 != null)
				return false;
		}
		else if (!isbn13.equals(other.isbn13))
			return false;
		if (listPrice == null)
		{
			if (other.listPrice != null)
				return false;
		}
		else if (!listPrice.equals(other.listPrice))
			return false;
		if (numOfCopies != other.numOfCopies)
			return false;
		if (numOfPages != other.numOfPages)
			return false;
		if (publisher == null)
		{
			if (other.publisher != null)
				return false;
		}
		else if (!publisher.equals(other.publisher))
			return false;
		if (removalStatus != other.removalStatus)
			return false;
		if (salePrice == null)
		{
			if (other.salePrice != null)
				return false;
		}
		else if (!salePrice.equals(other.salePrice))
			return false;
		if (title == null)
		{
			if (other.title != null)
				return false;
		}
		else if (!title.equals(other.title))
			return false;
		if (Double.doubleToLongBits(weight) != Double
				.doubleToLongBits(other.weight))
			return false;
		if (wholeSalePrice == null)
		{
			if (other.wholeSalePrice != null)
				return false;
		}
		else if (!wholeSalePrice.equals(other.wholeSalePrice))
			return false;
		return true;
	}
}