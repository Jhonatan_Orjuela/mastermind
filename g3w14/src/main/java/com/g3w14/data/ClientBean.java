package com.g3w14.data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

/**		Client Bean
 * All details of the client are here.
 * 
 * @author Sophie Leduc Major 0931442
 * @author Tyler Patricio 1140353
 * @author Theo
 *
 */
@Named("clientBean")
@SessionScoped
public class ClientBean implements Serializable {

	private static final long serialVersionUID = -4644598594514253880L;
	
	private int id;
	private String email;
	private String password;
	private String title;
	private String lastName;
	private String firstName;
	private String company;
	private String address1;
	private String address2;
	private String city;
	private String province;
	private String country;
	private String postalCode;
	private String homeTelephone;
	private String cellTelephone;
	private String lastSearch;
	private int admin;
	
	
	/**
	 * No-parameter constructor.
	 */
	public ClientBean() {
		email = "";
		password = "";
		title = "";
		lastName = "";
		firstName = "";
		company = "";
		address1 = "";
		address2 = "";
		city = "";
		province = "";
		country = "";
		postalCode = "";
		homeTelephone = "";
		cellTelephone = "";
		lastSearch = "none";
	}
	
	/**
	 * Parameter-filled constructor.
	 * 
	 * @param id
	 * @param email
	 * @param password
	 * @param title
	 * @param lastName
	 * @param firstName
	 * @param company
	 * @param address1
	 * @param address2
	 * @param city
	 * @param province
	 * @param country
	 * @param postalCode
	 * @param homeTelephone
	 * @param cellTelephone
	 * @param lastSearch
	 */
	public ClientBean(int id, String email, String password, String title,
			String lastName, String firstName, String company, String address1,
			String address2, String city, String province, String country,
			String postalCode,String homeTelephone,
			String cellTelephone, String lastSearch) {
		super();
		this.id = id;
		this.email = email;
		this.password = password;
		this.title = title;
		this.lastName = lastName;
		this.firstName = firstName;
		this.company = company;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
		this.homeTelephone = homeTelephone;
		this.cellTelephone = cellTelephone;
		this.lastSearch = lastSearch;
	}
	
	private static Map<String,Object> titlevalue;
	static{
		titlevalue = new LinkedHashMap<String,Object>();
		titlevalue.put("Mr.", "Mr"); //label, value
		titlevalue.put("Mrs.", "Mrs");
		titlevalue.put("Ms.", "Ms");
	}
 
	public Map<String,Object> getTitleValue() {
		return titlevalue;
	}
	
	private static Map<String,Object> provincevalue;
	static{
		provincevalue = new LinkedHashMap<String,Object>();
		provincevalue.put("Alberta", "AB"); //label, value
		provincevalue.put("British Columbia", "BC");
		provincevalue.put("Manitoba", "MB");
		provincevalue.put("Brunswick", "NB");
		provincevalue.put("Newfoundland and Labrador", "NL");
		provincevalue.put("Nova Scotia", "NS");
		provincevalue.put("Ontario", "ON");
		provincevalue.put("Prince Edward Island", "PE");
		provincevalue.put("Quebec", "QC");
		provincevalue.put("Saskatchewan", "SK");
		provincevalue.put("Northwest Territories", "NT");
		provincevalue.put("Nunavut", "NU");
		provincevalue.put("Yukon", "YT");
	}
 
	public Map<String,Object> getProvinceValue() {
		return provincevalue;
	}
	
	private static Map<String,Object> states;
	static{
		states = new LinkedHashMap<String,Object>();
		states.put("Alabama", "AL"); //label, value
		states.put("Alaska", "AK");
		states.put("Arizona", "AZ");
		states.put("Arkansas", "AR");
		states.put("California", "CA");
		states.put("Colorado", "CO");
		states.put("Connecticut", "CT");
		states.put("Delaware", "DE");
		states.put("Florida", "FL");
		states.put("Georgia", "GA");
		states.put("Hawaii", "HI");
		states.put("Idaho", "ID");
		states.put("Illinois", "IL");
		states.put("Indiana", "IN");
		states.put("Iowa", "IA");
		states.put("Kansas", "KS");
		states.put("Kentucky", "KY");
		states.put("Louisiana", "LA");
		states.put("Maine", "ME");
		states.put("Maryland", "MD");
		states.put("Massachusetts", "MA");
		states.put("Michigan", "MI");
		states.put("Minnesota", "MN");
		states.put("Mississippi", "MS");
		states.put("Missouri", "MO");
		states.put("Montana", "MT");
		states.put("Nebraska", "NE");
		states.put("Nevada", "NV");
		states.put("New Hampshire", "NH");
		states.put("New Jersey", "NJ");
		states.put("New Mexico", "NM");
		states.put("New York", "NY");
		states.put("North Carolina", "NC");
		states.put("North Dakota", "ND");
		states.put("Ohio", "OH");
		states.put("Oklahoma", "OK");
		states.put("Oregon", "OR");
		states.put("Pennsylvania", "PA");
		states.put("Rhode Island", "RI");
		states.put("South Carolina", "SC");
		states.put("South Dakota", "SD");
		states.put("Tennessee", "TN");
		states.put("Texas", "TX");
		states.put("Utah", "UT");
		states.put("Vermont", "VT");
		states.put("Virginia", "VA");
		states.put("Washington", "WA");
		states.put("West Virginia", "WV");
		states.put("Wisconsin", "WI");
		states.put("Wyoming", "WY");
	}
	
	public Map<String,Object> getState() {
		return states;
	}
	
	private static Map<String,Object> countryvalue;
	static{
		countryvalue = new LinkedHashMap<String,Object>();
		countryvalue.put("Canada", "Canada"); //label, value
//		countryvalue.put("United States of America", "USA");
	}
 
	public Map<String,Object> getCountryValue() {
		return countryvalue;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the province
	 */
	public String getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the homeTelephone
	 */
	public String getHomeTelephone() {
		return homeTelephone;
	}

	/**
	 * @param homeTelephone the homeTelephone to set
	 */
	public void setHomeTelephone(String homeTelephone) {
		this.homeTelephone = homeTelephone;
	}

	/**
	 * @return the cellTelephone
	 */
	public String getCellTelephone() {
		return cellTelephone;
	}

	/**
	 * @param cellTelephone the cellTelephone to set
	 */
	public void setCellTelephone(String cellTelephone) {
		this.cellTelephone = cellTelephone;
	}

	/**
	 * @return the lastSearch
	 */
	public String getLastSearch() {
		return lastSearch;
	}

	/**
	 * @param lastSearch the lastSearch to set
	 */
	public void setLastSearch(String lastSearch) {
		this.lastSearch = lastSearch;
	}


	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result
				+ ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + admin;
		result = prime * result
				+ ((cellTelephone == null) ? 0 : cellTelephone.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((homeTelephone == null) ? 0 : homeTelephone.hashCode());
		result = prime * result + id;
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((lastSearch == null) ? 0 : lastSearch.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result
				+ ((province == null) ? 0 : province.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientBean other = (ClientBean) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (admin != other.admin)
			return false;
		if (cellTelephone == null) {
			if (other.cellTelephone != null)
				return false;
		} else if (!cellTelephone.equals(other.cellTelephone))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (homeTelephone == null) {
			if (other.homeTelephone != null)
				return false;
		} else if (!homeTelephone.equals(other.homeTelephone))
			return false;
		if (id != other.id)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (lastSearch == null) {
			if (other.lastSearch != null)
				return false;
		} else if (!lastSearch.equals(other.lastSearch))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (province == null) {
			if (other.province != null)
				return false;
		} else if (!province.equals(other.province))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}


	
}