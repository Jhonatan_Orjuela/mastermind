package com.g3w14.data;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the
 * Survey table.
 * 
 * @author 1040134 Jhonatan Orjuela
 * This is the survey element, displayed on the site, used to gather a query on different topics
 */
@Named("surveyBean")
@RequestScoped
public class SurveyBean implements Serializable {

	private static final long serialVersionUID = 5365614225485479393L;
	private int surveyId;
	private String question;
	private String option1;	
	private int votes1;
	private String option2;	
	private int votes2;
	private String option3;	
	private int votes3;
	private String option4;	
	private int votes4;
	private int totalVotes;
	private boolean disable ;
	/**
	 * @param surveyId
	 * @param question
	 * @param option1
	 * @param votes1
	 * @param option2
	 * @param votes2
	 * @param option3
	 * @param votes3
	 * @param option4
	 * @param votes4
	 * @param totalVotes
	 * @param disable
	 */
	public SurveyBean(int surveyId, String question, String option1,
			int votes1, String option2, int votes2, String option3, int votes3,
			String option4, int votes4, int totalVotes, boolean disable) {
		super();
		this.surveyId = surveyId;
		this.question = question;
		this.option1 = option1;
		this.votes1 = votes1;
		this.option2 = option2;
		this.votes2 = votes2;
		this.option3 = option3;
		this.votes3 = votes3;
		this.option4 = option4;
		this.votes4 = votes4;
		this.totalVotes = totalVotes;
		this.disable = disable;
	}



	/**
	 * No-parameter constructor.
	 */
	public SurveyBean()
	{
		this.question = "";
		this.option1 = "";
		this.votes1 = 0;
		this.option2 = "";
		this.votes2 = 0;
		this.option3 = "";
		this.votes3 = 0;
		this.option4 = "";
		this.votes4 = 0;
		this.totalVotes = 0;
		this.disable = true;
	}

	/**
	 * @return the surveyId
	 */
	public int getSurveyId() {
		return surveyId;
	}


	/**
	 * @param surveyId the surveyId to set
	 */
	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}


	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}


	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}


	/**
	 * @return the option1
	 */
	public String getOption1() {
		return option1;
	}


	/**
	 * @param option1 the option1 to set
	 */
	public void setOption1(String option1) {
		this.option1 = option1;
	}


	/**
	 * @return the votes1
	 */
	public int getVotes1() {
		return votes1;
	}


	/**
	 * @param votes1 the votes1 to set
	 */
	public void setVotes1(int votes1) {
		this.votes1 = votes1;
	}


	/**
	 * @return the option2
	 */
	public String getOption2() {
		return option2;
	}


	/**
	 * @param option2 the option2 to set
	 */
	public void setOption2(String option2) {
		this.option2 = option2;
	}


	/**
	 * @return the votes2
	 */
	public int getVotes2() {
		return votes2;
	}


	/**
	 * @param votes2 the votes2 to set
	 */
	public void setVotes2(int votes2) {
		this.votes2 = votes2;
	}


	/**
	 * @return the option3
	 */
	public String getOption3() {
		return option3;
	}


	/**
	 * @param option3 the option3 to set
	 */
	public void setOption3(String option3) {
		this.option3 = option3;
	}


	/**
	 * @return the votes3
	 */
	public int getVotes3() {
		return votes3;
	}


	/**
	 * @param votes3 the votes3 to set
	 */
	public void setVotes3(int votes3) {
		this.votes3 = votes3;
	}


	/**
	 * @return the option4
	 */
	public String getOption4() {
		return option4;
	}


	/**
	 * @param option4 the option4 to set
	 */
	public void setOption4(String option4) {
		this.option4 = option4;
	}


	/**
	 * @return the votes4
	 */
	public int getVotes4() {
		return votes4;
	}


	/**
	 * @param votes4 the votes4 to set
	 */
	public void setVotes4(int votes4) {
		this.votes4 = votes4;
	}


	/**
	 * @return the totalVotes
	 */
	public int getTotalVotes() {
		return totalVotes;
	}


	/**
	 * @param totalVotes the totalVotes to set
	 */
	public void setTotalVotes(int totalVotes) {
		this.totalVotes = totalVotes;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SurveyBean [surveyId=" + surveyId + ", question=" + question
				+ ", option1=" + option1 + ", votes1=" + votes1 + ", option2="
				+ option2 + ", votes2=" + votes2 + ", option3=" + option3
				+ ", votes3=" + votes3 + ", option4=" + option4 + ", votes4="
				+ votes4 + ", totalVotes=" + totalVotes + ", disable="
				+ disable + "]";
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (disable ? 1231 : 1237);
		result = prime * result + ((option1 == null) ? 0 : option1.hashCode());
		result = prime * result + ((option2 == null) ? 0 : option2.hashCode());
		result = prime * result + ((option3 == null) ? 0 : option3.hashCode());
		result = prime * result + ((option4 == null) ? 0 : option4.hashCode());
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		result = prime * result + surveyId;
		result = prime * result + totalVotes;
		result = prime * result + votes1;
		result = prime * result + votes2;
		result = prime * result + votes3;
		result = prime * result + votes4;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SurveyBean other = (SurveyBean) obj;
		if (disable != other.disable)
			return false;
		if (option1 == null) {
			if (other.option1 != null)
				return false;
		} else if (!option1.equals(other.option1))
			return false;
		if (option2 == null) {
			if (other.option2 != null)
				return false;
		} else if (!option2.equals(other.option2))
			return false;
		if (option3 == null) {
			if (other.option3 != null)
				return false;
		} else if (!option3.equals(other.option3))
			return false;
		if (option4 == null) {
			if (other.option4 != null)
				return false;
		} else if (!option4.equals(other.option4))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (surveyId != other.surveyId)
			return false;
		if (totalVotes != other.totalVotes)
			return false;
		if (votes1 != other.votes1)
			return false;
		if (votes2 != other.votes2)
			return false;
		if (votes3 != other.votes3)
			return false;
		if (votes4 != other.votes4)
			return false;
		return true;
	}

	/**
	 * @return the disable
	 */
	public boolean getDisable() {
		return disable;
	}

	/**
	 * @param disable the disable to set
	 */
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
}