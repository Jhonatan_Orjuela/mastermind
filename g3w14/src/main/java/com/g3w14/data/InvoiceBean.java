package com.g3w14.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the
 * Invoice table.
 * 
 * @author Tyler
 *
 */
@Named("invoiceBean")
@RequestScoped
public class InvoiceBean implements Serializable
{
	private int saleNumber;
	private Timestamp date; 
	private String clientNumber;
	private BigDecimal totalNet;
	private int idpst;
	private final BigDecimal GST = new BigDecimal(5.00);
	private BigDecimal totalGross;
		
	/**
	 * No-parameter constructor.
	 */
	public InvoiceBean() {
		saleNumber = 0;
		date = null;
		clientNumber = "";
		totalNet = new BigDecimal(-1.00);
		idpst = 0;
		totalGross = new BigDecimal(-1.00);
	}
	
	/**
	 * Parameter-filled constructor.
	 * 
	 * @param saleNumber
	 * @param date
	 * @param clientNumber
	 * @param totalNet
	 * @param idpst
	 * @param totalGross
	 */
	public InvoiceBean(int saleNumber, Timestamp date, String clientNumber,
			BigDecimal totalNet, int idpst, BigDecimal totalGross)
	{
		super();
		this.saleNumber = saleNumber;
		this.date = date;
		this.clientNumber = clientNumber;
		this.totalNet = totalNet;
		this.idpst = idpst;
		this.totalGross = totalGross;
	}

	/**
	 * @return the saleNumber
	 */
	public int getSaleNumber() {
		return saleNumber;
	}

	/**
	 * @param saleNumber the saleNumber to set
	 */
	public void setSaleNumber(int saleNumber) {
		this.saleNumber = saleNumber;
	}

	/**
	 * @return the date
	 */
	public Timestamp getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Timestamp date) {
		this.date = date;
	}

	/**
	 * @return the clientNumber
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * @param clientNumber the clientNumber to set
	 */
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}

	/**
	 * @return the totalNet
	 */
	public BigDecimal getTotalNet() {
		return totalNet;
	}

	/**
	 * @param totalNet the totalNet to set
	 */
	public void setTotalNet(BigDecimal totalNet) {
		this.totalNet = totalNet;
	}

	/**
	 * @return the idpst
	 */
	public int getIdpst() {
		return idpst;
	}

	/**
	 * @param idpst the idpst to set
	 */
	public void setIdpst(int idpst) {
		this.idpst = idpst;
	}

	/**
	 * @return the totalGross
	 */
	public BigDecimal getTotalGross() {
		return totalGross;
	}

	/**
	 * @param totalGross the totalGross to set
	 */
	public void setTotalGross(BigDecimal totalGross) {
		this.totalGross = totalGross;
	}

	/**
	 * @return the gST
	 */
	public BigDecimal getGST() {
		return GST;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((GST == null) ? 0 : GST.hashCode());
		result = prime * result
				+ ((clientNumber == null) ? 0 : clientNumber.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + idpst;
		result = prime * result + saleNumber;
		result = prime * result
				+ ((totalGross == null) ? 0 : totalGross.hashCode());
		result = prime * result
				+ ((totalNet == null) ? 0 : totalNet.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceBean other = (InvoiceBean) obj;
		if (GST == null) {
			if (other.GST != null)
				return false;
		} else if (!GST.equals(other.GST))
			return false;
		if (clientNumber == null) {
			if (other.clientNumber != null)
				return false;
		} else if (!clientNumber.equals(other.clientNumber))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (idpst != other.idpst)
			return false;
		if (saleNumber != other.saleNumber)
			return false;
		if (totalGross == null) {
			if (other.totalGross != null)
				return false;
		} else if (!totalGross.equals(other.totalGross))
			return false;
		if (totalNet == null) {
			if (other.totalNet != null)
				return false;
		} else if (!totalNet.equals(other.totalNet))
			return false;
		return true;
	}
}