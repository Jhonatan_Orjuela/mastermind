package com.g3w14.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Bean class that denotes all of the variables relating to the 
 * Taxes table.
 * 
 * @author Tyler Patricio
 */
@Named("taxBean")
@RequestScoped
public class TaxBean implements Serializable
{
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	private int id;
	private String province;
	private BigDecimal pst;
	
	/**
	 * No-parameter constructor.
	 */
	public TaxBean()
	{
		this.province = "";
		this.pst = new BigDecimal(-1.00);
	}

	/**
	 * Parameter-filled constructor.
	 * 
	 * @param id
	 * @param province
	 * @param pst
	 */
	public TaxBean(String province, BigDecimal pst)
	{
		this.province = province;
		this.pst = pst;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the province
	 */
	public String getProvince()
	{
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(String province)
	{
		this.province = province;
	}

	/**
	 * @return the pst
	 */
	public BigDecimal getPst()
	{
		return pst;
	}

	/**
	 * @param pst the pst to set
	 */
	public void setPst(BigDecimal pst)
	{
		this.pst = pst;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "TaxBean [id=" + id + ", province=" + province + ", pst=" + pst
				+ "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result
				+ ((province == null) ? 0 : province.hashCode());
		result = prime * result + ((pst == null) ? 0 : pst.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxBean other = (TaxBean) obj;
		if (id != other.id)
			return false;
		if (province == null)
		{
			if (other.province != null)
				return false;
		}
		else if (!province.equals(other.province))
			return false;
		if (pst == null)
		{
			if (other.pst != null)
				return false;
		}
		else if (!pst.equals(other.pst))
			return false;
		return true;
	}
}