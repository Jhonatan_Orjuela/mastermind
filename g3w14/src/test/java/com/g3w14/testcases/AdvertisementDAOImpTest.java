package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.sql.SQLException;

import org.jboss.arquillian.container.test.api.Deployment;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.AdvertisementBean;
import com.g3w14.persistence.AdvertisementDAOImp;

/**
 * JUnit testing cases for AdvertisementDAOImp class. 
 * 
 * @author Sean-Frankel Gaon Canlas
 * @author Sophie Leduc Major
 *
 */
@RunWith(Arquillian.class)
public class AdvertisementDAOImpTest {
	@Inject
	AdvertisementDAOImp adao;

	@Deployment
	public static WebArchive deploy()
	{		
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(AdvertisementDAOImp.class.getPackage())
				.addPackage(AdvertisementBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}

	@Test
	public void testQueryAllAds() throws SQLException {
		
		assertEquals(1, adao.getQueryRecords().size());

	}

	@Test
	public void testQuerySingleAdNormal() throws SQLException {
		
		assertEquals(1, adao.getSpecificQueryRecords("Chicken4Gold.jpg").size());
	}

	@Test
	public void testQuerySingleAdNoSuchAd() throws SQLException {

		assertEquals(0, adao.getSpecificQueryRecords("poop").size());
	}

	@Test
	public void testInsertAdNormal() throws SQLException {
		AdvertisementBean a = new AdvertisementBean();
		a.setAdFileName("freeIpad.png");
		a.setUrl("http://scam.com");
		
		assertEquals(1, adao.insertRecord(a));
		
		adao.deleteRecord(adao.getSpecificQueryRecords("freeIpad.png").get(0));
		

	}

	@Test (expected=SQLException.class)
	public void testInsertAdExistingURL() throws SQLException {
		
		AdvertisementBean a = new AdvertisementBean();
		a.setAdFileName("lolwhut");
		a.setUrl("http://exchangeChicken4Gold.com");
		adao.insertRecord(a);
		

	}

	@Test
	public void testUpdateAdNormal() throws SQLException{
		
		AdvertisementBean a = new AdvertisementBean();

		a.setAdFileName("something.png");
		a.setUrl("http://herp.com");
		
		adao.insertRecord(a);
		
		AdvertisementBean b = new AdvertisementBean();
		b.setAdFileName("somethingelse.png");
		
		int id = adao.getSpecificQueryRecords("something.png").get(0).getAdId();
		b.setAdId(id);

		assertEquals(1, adao.updateRecord(b));
		
		adao.deleteRecord(b);
		

	}

	@Test
	public void testUpdateClientNoSuchClient() throws SQLException {

		AdvertisementBean a = new AdvertisementBean();
		a.setAdId(999);
		a.setAdFileName("something.png");
		a.setUrl("http://herp.com");
		
		assertEquals(0,adao.updateRecord(a));
	}

	@Test
	public void testDeleteClientNormal() throws SQLException{
		AdvertisementBean a = new AdvertisementBean();
		a.setAdFileName("eatChocolat.png");
		a.setUrl("http://chocolatisgood.com");
		
		adao.insertRecord(a);
		
		int id = adao.getSpecificQueryRecords("eatChocolat.png").get(0).getAdId();
		
		a.setAdId(id);
		
		assertEquals(1, adao.deleteRecord(a));

	}

	@Test 
	public void testDeleteClientNoSuchClient() throws SQLException {
		AdvertisementBean a = new AdvertisementBean();
		a.setAdId(999);
		a.setAdFileName("playThisShitMMO.gif");
		a.setUrl("http://TittiesMMO.com");
		
		assertEquals(0, adao.deleteRecord(a));
	}
}