package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.g3w14.data.TaxBean;
import com.g3w14.persistence.TaxDAOImp;

/**
 * JUnit testing cases for TaxDAOImp class.
 * 
 * @author Tyler Patricio
 */
@RunWith(Arquillian.class)
public class TaxDAOImpTest
{
	@Inject
	TaxDAOImp tdi;
	
	TaxBean tb;
	ArrayList<TaxBean> tbList;

	@Deployment
	public static WebArchive deploy()
	{
		final File[] dependencies = Maven
				.resolver()
				.loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java",
						"org.assertj:assertj-core").withoutTransitivity()
				.asFile();

		final WebArchive webArchive = ShrinkWrap.create(WebArchive.class)
				.addPackage(TaxDAOImp.class.getPackage())
				.addPackage(TaxBean.class.getPackage())
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.addAsWebInfResource("resources-mysql-ds.xml", "resources.xml")
				.addAsLibraries(dependencies);

		return webArchive;
	}
	
	@Before
	public void init() throws SQLException
	{
		tb = new TaxBean();
		tbList = new ArrayList<TaxBean>();

		tb.setProvince("QC");
		tb.setPst(new BigDecimal(9.97));
	}

	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#returnAllRecords()}.
	 * @throws SQLException 
	 */
	@Test//(timeout = 2000)
	public void testReturnAllRecords() throws SQLException
	{
		tbList = tdi.returnAllRecords();

		// check current amount of records
		assertEquals("Total records in database: 4", 4, tbList.size());
	}

	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#advancedSearch(com.g3w14.data.TaxBean)}.
	 */
	/*
	@Test(timeout = 2000)
	public void testAdvancedSearch() throws SQLException
	{
		tb.setId(2);
		tb.setProvince("QC");
		tb.setPst(new BigDecimal(9.97));
		System.out.println("*************" + tb.toString());
		assertEquals("Expected 1", 1 , tdi.advancedSearch(tb).size());
	}
*/
	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#deleteRecord(int)}.
	 * @throws SQLException 
	 */
	@Test//(timeout = 2000)
	public void testDeleteRecord() throws SQLException
	{
		int resultNum;

		// delete the third record
		resultNum = tdi.deleteRecord(1);

		assertEquals("The number 1 is expected here:", 1, resultNum);
	}

	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#deleteRecord(int)}.
	 * @throws SQLException 
	 */
	@Test//(timeout = 2000)
	public void testDeleteNonExistantRecord() throws SQLException
	{
		int resultNum;

		// delete the third record
		resultNum = tdi.deleteRecord(10000);

		assertEquals("The number 0 is expected here:", 0, resultNum);
	}
	
	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#insertRecord(com.g3w14.data.TaxBean)}.
	 * @throws SQLException 
	 */
	@Test//(timeout = 5000)
	public void testInsertRecord() throws SQLException
	{
		int resultNum;

		// insert the test record
		resultNum = tdi.insertRecord(tb);

		assertEquals("The number 1 is expected here:", 1, resultNum);
	}

	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#insertRecord(com.g3w14.data.TaxBean)}.
	 * @throws SQLException 
	 */
	@Test(/*timeout = 2000, */expected = NullPointerException.class)
	public void testInsertRecordWithNull() throws SQLException
	{
		@SuppressWarnings("unused")
		int resultNum;

		// insert the test record
		resultNum = tdi.insertRecord(null);
	}
	
	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#updateRecord(com.g3w14.data.TaxBean)}.
	 * @throws SQLException 
	 */
	@Test//(timeout = 2000)
	public void testUpdateRecord() throws SQLException
	{
		int resultNum;
		
		tb.setId(1);
		

		resultNum = tdi.updateRecord(tb);
		assertEquals("The number 1 is expected here:", 1, resultNum);
	}
	
	/**
	 * Test method for {@link com.g3w14.persistence.TaxDAOImp#updateRecord(com.g3w14.data.TaxBean)}.
	 * @throws SQLException 
	 */
	@Test(/*timeout = 2000, */expected = NullPointerException.class)
	public void testUpdateRecordWithNull() throws SQLException
	{
		// update with null
		@SuppressWarnings("unused")
		int resultNum = tdi.updateRecord(null);
	}
}