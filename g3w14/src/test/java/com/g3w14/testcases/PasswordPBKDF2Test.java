package com.g3w14.testcases;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.junit.Before;
import org.junit.Test;

import com.g3w14.data.PasswordPBKDF2;

/**
 * JUnit testing cases for PasswordPBKDF2 class.
 * 
 * @author 0931442
 *
 */
public class PasswordPBKDF2Test {
	String password;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		password = PasswordPBKDF2.createPassword("password");
		
	}

	@Test
	public void testRightPassword() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String wrongPassword = "wrong";
		
		assertFalse( PasswordPBKDF2.validatePassword(wrongPassword, password));
	}
	
	@Test
	public void testWrongPassword() throws NoSuchAlgorithmException, InvalidKeySpecException {
		String rightPassword = "password";
		
		assertTrue( PasswordPBKDF2.validatePassword(rightPassword, password));
	}

}
