package com.expresionless.jokeapp;

import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

/**
 * Database helper used to construct the Category table. Performs other
 * operations such as inserting and deleting.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class CategoryDBHelper {
	private final static String DB_NAME = "category.db";
	private final static int DB_VERSION = 1;
	private final static String TABLE_CATEGORY = "Category";
	private String path = "data/data/com.expresionless.jokeapp/databases/";
	private File dbFile;

	// columns for Category table
	private final static String COL_ID = "_id";
	private final static String COL_CATEGORY = "category";
	private final static String COL_SUBCATEGORY = "subCategory";
	private final static String COL_NUM_OF_JOKES = "numOfJokes";
	private DatabaseOpenHelper dbOpenHelper;
	@SuppressWarnings("unused")
	private Context context;

	/**
	 * Constructor that receives the context.
	 * 
	 * @param context
	 */
	public CategoryDBHelper(Context context) {
		this.context = context;
		this.dbOpenHelper = new DatabaseOpenHelper(context, DB_NAME, null,
				DB_VERSION);
		populateDatabase();
	}

	/////////////////////// CRUD METHODS ///////////////////////

	/**
	 * Deletes a category from the database.
	 * 
	 * @param id
	 *            the id of the joke to be deleted
	 */
	public void deleteCategory(int id) {
		dbOpenHelper.getWritableDatabase().delete(TABLE_CATEGORY,
				COL_ID + "=?", new String[] { String.valueOf(id) });
	}

	/**
	 * Queries every record in the Category table.
	 * 
	 * @return the cursor containing every record
	 */
	public Cursor getCategories() {
		return dbOpenHelper.getWritableDatabase().query(TABLE_CATEGORY, null,
				null, null, null, null, null);
	}

	/**
	 * Returns the category column.
	 * 
	 * @return the colCategory
	 */
	public static String getColCategory() {
		return COL_CATEGORY;
	}

	/**
	 * Inserts a new category as a record in the Category table.
	 * 
	 * @param category
	 * @param subCategory
	 * @param numOfJokes
	 * @return
	 */
	public long insertNewCategory(String category, String subCategory,
			int numOfJokes) {
		ContentValues cv = new ContentValues();

		cv.put(getColCategory(), category);
		cv.put(COL_SUBCATEGORY, subCategory);
		cv.put(COL_NUM_OF_JOKES, numOfJokes);

		long code = dbOpenHelper.getWritableDatabase().insert(TABLE_CATEGORY,
				null, cv);

		return code;
	}

	/**
	 * Populates the database with the corresponding categories.
	 */
	private void populateDatabase() 
	{
		dbFile = new File(path + DB_NAME);
		
		if (!dbFile.exists()) 
		{
			insertNewCategory("Geek", "Science", 2);
			insertNewCategory("Geek", "Computer Science", 4);
			insertNewCategory("Sports", "Golf", 3);
			insertNewCategory("Sports", "Hockey", 2);
			insertNewCategory("Work", "Secretary", 2);
			insertNewCategory("Work", "Intern", 2);
			insertNewCategory("Work", "Boss", 2);
		}
	}
	
	/**
	 * Updates the values in the database.
	 * 
	 * @param id
	 * @param category
	 * @param subCategory
	 * @param numOfJokes
	 * @return
	 */
	public long update(String id, String category, String subCategory, int numOfJokes) 
	{
		ContentValues cv = new ContentValues();

		cv.put(COL_CATEGORY, category);
		cv.put(COL_SUBCATEGORY, subCategory);
		cv.put(COL_NUM_OF_JOKES, numOfJokes);

		long code = dbOpenHelper.getWritableDatabase().update(TABLE_CATEGORY, cv,
				COL_ID + "= ?", new String[] { id });

		return code;
	}

	/**
	 * Inner class containing the Database life-cycle methods.
	 * 
	 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
	 */
	private class DatabaseOpenHelper extends SQLiteOpenHelper
	{
		public DatabaseOpenHelper(Context context, String name,
				CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		/**
		 * Creates the Joke table.
		 */
		@Override
		public void onCreate(SQLiteDatabase db) 
		{
			String sql_grade = "CREATE TABLE " + TABLE_CATEGORY + "(" + COL_ID
					+ " integer primary key autoincrement, " + getColCategory()
					+ " varchar(25) not null, " + COL_SUBCATEGORY
					+ " varchar(25) not null, " + COL_NUM_OF_JOKES
					+ " integer(3) not null " + ");";

			db.execSQL(sql_grade);
		}

		/**
		 * Changes the defined version to a later version.
		 * 
		 * @param db
		 * @param oldVersion
		 * @param newVersion
		 */
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
		{
			Log.w(JokeDBHelper.class.getName(),
					"Upgrading database from version " + oldVersion + " to "
							+ newVersion + ", which will destroy all old data.");

			db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
			onCreate(db);
		}
	}
}