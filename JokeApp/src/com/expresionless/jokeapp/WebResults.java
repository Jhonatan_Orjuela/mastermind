package com.expresionless.jokeapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Activity used to show results from searching.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class WebResults extends Activity 
{
	private ArrayList<String> ids;
	private ArrayList<String> descriptions;
	private ListView jokes;
	private String jokeDesc;
	private String selectedId;
	private Bundle jokeBundle;
	private Context context;
	@SuppressWarnings("unused")
	private TextView noJokeText;
	
	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.web_results);
		jokeBundle = new Bundle();
		context = this;
		
		//extract the results form the bundle in the intent
		ids = getIntent().getExtras().getBundle("results").getStringArrayList("id");
		descriptions = getIntent().getExtras().getBundle("results").getStringArrayList("descriptions");
		jokes = (ListView) findViewById(R.id.jokeListView);
		
		if (descriptions.isEmpty()){
			System.out.println("it passes through here");
			noJokeText = (TextView) findViewById(R.id.noJokesfoundText);
			jokes.setVisibility(ListView.GONE);
		}
		
		else
		{
			System.out.println("it shouldn't go here");

			jokes = (ListView) findViewById(R.id.jokeListView);
			jokes.setAdapter(new ArrayAdapter<String>(this,
					R.layout.category_list_item, descriptions));
			jokes.setOnItemClickListener(new OnItemClickListener()
			{
				/**
				 * Launches the ShowJoke activity.
				 */
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					jokeDesc = ((TextView) view).getText().toString();

					for(int i = 0; i < descriptions.size(); i++)
					{
						if(descriptions.get(i).equals(jokeDesc))
							selectedId = ids.get(i);
					}
						
					// create the ShowWebJoke Intent
					Intent intent = new Intent(context, ShowWebJoke.class);
					jokeBundle.putString("id", selectedId);
					intent.putExtra("idBundle", jokeBundle);
					
					startActivity(intent);
				}
			});
		}
	}
}