package com.expresionless.jokeapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Presents the status of the database by means of a ListView pertaining to the
 * categories.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class DBStatus extends Activity {
	private CategoryDBHelper categoryHelper;
	private JokeDBHelper jokeHelper;
	private Cursor cursor;
	private Cursor cursorDate;
	
	private TextView geekCategoryCount;
	private TextView sportsCategoryCount;
	private TextView workCategoryCount;
	private TextView firstDate;
	private TextView lastDate;

	/**
	 * Creates and sets the appropriate layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.db_status);

		jokeHelper = new JokeDBHelper(this);
		categoryHelper = new CategoryDBHelper(this);

		cursor = categoryHelper.getCategories();
		
		cursor.moveToFirst();
		int geekCount = Integer.parseInt(cursor.getString(3));
		geekCategoryCount = (TextView) findViewById(R.id.geekJokeCount);
		cursor.moveToNext();
		geekCount += Integer.parseInt(cursor.getString(3));
		geekCategoryCount.setText(Integer.toString(geekCount));
		cursor.moveToNext();
		
		int sportsCount = Integer.parseInt(cursor.getString(3));
		sportsCategoryCount = (TextView) findViewById(R.id.sportsJokeCount);
		cursor.moveToNext();
		sportsCount += Integer.parseInt(cursor.getString(3));
		sportsCategoryCount.setText(Integer.toString(sportsCount));
		cursor.moveToNext();
		
		int workCount = Integer.parseInt(cursor.getString(3));
		workCategoryCount = (TextView) findViewById(R.id.workJokeCount);
		cursor.moveToNext();
		workCount += Integer.parseInt(cursor.getString(3));
		cursor.moveToNext();
		workCount += Integer.parseInt(cursor.getString(3));
		workCategoryCount.setText(Integer.toString(workCount));

		// Date Cursor.
		cursorDate = jokeHelper.getJokes();
		cursorDate.moveToFirst();

		firstDate = (TextView) findViewById(R.id.dateFirstAdded);
		lastDate = (TextView) findViewById(R.id.dateLastAdded);
		

		if (cursorDate != null && cursorDate.getCount() > 0) {
			firstDate.setText(cursorDate.getString(cursorDate
				.getColumnIndex("createDate")));
			cursorDate.moveToLast();
			lastDate.setText(cursorDate.getString(cursorDate
					.getColumnIndex("createDate")));
		}
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		DBStatus.this
				.startActivity(new Intent(DBStatus.this, Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		DBStatus.this.startActivity(new Intent(DBStatus.this, About.class));
	}
}