package com.expresionless.jokeapp;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * MainActivity class used to create and launch the main application.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class MainActivity extends Activity
{
	private static final String PREFS_NAME = "PrefsFile";
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;

	private ListView jokeListView;
	private JokeDBHelper jokeDB;
	@SuppressWarnings("unused")
	private SimpleCursorAdapter sca;
	@SuppressWarnings("unused")
	private CategoryDBHelper categoryHelper;
	private String jokeDesc;
	private String[] categories;
	private boolean isRandomized;
	private String[] fiveJokes;
	private String edited;
	
	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		categoryHelper = new CategoryDBHelper(this);

		categories = new String[3];
		jokeDB = new JokeDBHelper(this);
		prefs = getSharedPreferences(PREFS_NAME, 0);
		editor = prefs.edit();

		jokeListView = (ListView) findViewById(R.id.jokeListView);
		jokeListView.setOnItemClickListener(new OnItemClickListener()
		{
			/**
			 * Launches the ShowJoke activity.
			 */
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id)
			{
				jokeDesc = ((TextView) view).getText().toString();
				MainActivity.this.startActivity(new Intent(MainActivity.this,
						ShowJoke.class));
			}
		});
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle item selection
		switch (item.getItemId())
		{
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences()
	{
		MainActivity.this.startActivity(new Intent(MainActivity.this,
				Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout()
	{
		MainActivity.this.startActivity(new Intent(MainActivity.this,
				About.class));
	}

	/**
	 * Adds a joke to the ListView.
	 * 
	 * @param v
	 */
	public void onAddJokeClick(View v)
	{
		MainActivity.this.startActivity(new Intent(MainActivity.this,
				JokeForm.class));
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		savedInstanceState.putString("Joke", jokeDesc);
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onPause()
	{
		// storing the joke description in the SharedPreferences
		super.onPause();
		editor.putString("Joke", jokeDesc);
		editor.putString("newDesc", "-1");
		editor.commit();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		prefs = getSharedPreferences(PREFS_NAME, 0);
		
		if(prefs.getBoolean("deleted", false))
			removeDeletedJoke();
		
		edited = prefs.getString("newDesc", "-1");
		System.out.println(edited);
		
		if(!edited.equals("-1"))
			updateEditedDescription();
		
		if (!isRandomized)
		{
			displayJokes();
			isRandomized = true;
		}
	}

	/**
	 * Updates the description that was edited.
	 */
	private void updateEditedDescription() 
	{
		for(int i = 0 ; i < fiveJokes.length;i++)
		{
			if(fiveJokes[i].equals(jokeDesc))
				fiveJokes[i] = edited;
		}
			
		jokeListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.category_list_item, fiveJokes));
	}

	/**
	 * Removes the previously delete jokes from the list view.
	 */
	private void removeDeletedJoke()
	{
		int toExclude = -1;
		String[] sudoJokes = new String[fiveJokes.length -1];
		int j = 0;
		
		for (int i = 0; i < fiveJokes.length; i++)
		{
			if (fiveJokes[i].equals(prefs.getString("Joke", "none")))
				toExclude = i;
		}
				
		
		for (int i = 0; i < fiveJokes.length; i++)
		{
			if (i != toExclude)
			{
				sudoJokes[j] = fiveJokes[i];
				j++;
			}
		}
		
		fiveJokes = sudoJokes;
		editor.putBoolean("deleted", false);
		editor.commit();
		jokeListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.category_list_item, sudoJokes));
	}

	/**
	 * Handles the preferences set and modifies the UI accordingly.
	 */
	private void displayJokes()
	{
		ArrayList<Integer> positionIndices = new ArrayList<Integer>();

		int categoryCounter;
		int max = 0;
		int randomPosition;
		final int CATEGORY_LENGTH = 5;
		int limit = 0;

		String[] from = { jokeDB.getColJokeShortDescription() };
		int[] to = { R.id.list_item };

		categoryCounter = determinePrefSettings();

		// if there is something present in the category array
		if (categoryCounter != 0)
		{
			Cursor jokesByCategory = jokeDB.getJokeByCategory(categories);
			max = getMaximumNumberOfJokes(jokesByCategory);

			System.out.println("max: " + max);
			
			// populate
			if (max < CATEGORY_LENGTH) {
				fiveJokes = new String[max];
				limit = max;
			}

			else
			{
				fiveJokes = new String[CATEGORY_LENGTH];
				limit = CATEGORY_LENGTH;
			}

			for (int i = 0; i < limit; i++)
			{
				randomPosition = (int) (Math.random() * max);

				if (i != 0)
				{
					// loop until you get a unique position
					while (positionIndices.contains(randomPosition))
						randomPosition = (int) (Math.random() * max);
				}

				positionIndices.add(randomPosition);
				jokesByCategory.moveToPosition(randomPosition);
				fiveJokes[i] = jokesByCategory.getString(jokesByCategory
						.getColumnIndex("shortDescription"));
				jokesByCategory.moveToNext();
			}

			positionIndices.clear();

			sca = new SimpleCursorAdapter(this, R.layout.category_list_item,
					jokesByCategory, from, to, 0);
			jokeListView.setAdapter(new ArrayAdapter<String>(this,
					R.layout.category_list_item, fiveJokes));
		}

		else
		{
			fiveJokes = new String[CATEGORY_LENGTH];

			Cursor jokes = jokeDB.getJokes();
			max = getMaximumNumberOfJokes(jokes);

			for (int i = 0; i < CATEGORY_LENGTH; i++)
			{
				randomPosition = (int) (Math.random() * max);

				if (i != 0)
				{
					// loop until you get a unique position
					while (positionIndices.contains(randomPosition))
						randomPosition = (int) (Math.random() * max);
				}

				positionIndices.add(randomPosition);
				jokes.moveToPosition(randomPosition);

				fiveJokes[i] = jokes.getString(jokes
						.getColumnIndex("shortDescription"));
				jokes.moveToNext();
			}

			positionIndices.clear();

			jokeListView.setAdapter(new ArrayAdapter<String>(this,
					R.layout.category_list_item, fiveJokes));
		}
	}

	/**
	 * Analyzes the preferences and infers the number of categories to be set.
	 * 
	 * @return the count for the number of categories
	 */
	private int determinePrefSettings()
	{
		String[] categoryPrefNames = { "geekCategory", "sportsCategory",
				"workCategory" };
		String[] categoryNames = { "Geek", "Sports", "Work" };
		int randomPosition;
		int max;
		int categoryCounter = 0;

		if (prefs.getBoolean("startUp", false))
		{
			Cursor jokes = jokeDB.getJokes();
			max = getMaximumNumberOfJokes(jokes);

			randomPosition = (int) (Math.random() * max);
			jokes.moveToPosition(randomPosition);
			jokeDesc = jokes
					.getString(jokes.getColumnIndex("shortDescription"));

			MainActivity.this.startActivity(new Intent(MainActivity.this,
					ShowJoke.class));
		}

		// determine which preferences are set and assign the categories to the array
		for (int i = 0; i < categoryPrefNames.length; i++)
		{
			if (prefs.getBoolean(categoryPrefNames[i], true))
			{
				categories[categoryCounter] = categoryNames[i];
				categoryCounter++;
			}
		}

		return categoryCounter;
	}

	/**
	 * Acquires the maximum number of jokes contained within the cursor.
	 * 
	 * @return the maximum number of jokes
	 */
	private int getMaximumNumberOfJokes(Cursor cursor)
	{
		int max = 0;

		// get maximum number of jokes and move to the first one in the cursor
		while (cursor.moveToNext())
			max++;
		cursor.moveToFirst();

		return max;
	}

	/**
	 * Launches the DB Status layout.
	 * 
	 * @param v
	 */
	public void onDBStatusClick(View v)
	{
		MainActivity.this.startActivity(new Intent(MainActivity.this,
				DBStatus.class));
	}
	
	/**
	 * Event handler that randomizes the jokes in the ListView.
	 * 
	 * @param v
	 */
	public void onRandomizeClick(View v) 
	{
		displayJokes();
	}
	
	/**
	 * Event handler that takes care of searching for a joke.
	 * 
	 * @param v
	 */
	public void onSearchClick(View v)
	{
		MainActivity.this.startActivity(new Intent(MainActivity.this,
				WebSearch.class));
	}
}