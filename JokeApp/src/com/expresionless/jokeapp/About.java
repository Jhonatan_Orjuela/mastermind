package com.expresionless.jokeapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * About activity used to display information about the app.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class About extends Activity {
	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
	}

	/**
	 * Creates the options menu.
	 * 
	 * @param menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Determines the appropriate UI to launch depending on what was initiated
	 * by the user.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.settings:
			openPreferences();
			return true;
		case R.id.about:
			openAbout();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Opens the Preferences UI.
	 */
	public void openPreferences() {
		About.this.startActivity(new Intent(About.this, Preferences.class));
	}

	/**
	 * Opens the About UI.
	 */
	public void openAbout() {
		About.this.startActivity(new Intent(About.this, About.class));
	}
}