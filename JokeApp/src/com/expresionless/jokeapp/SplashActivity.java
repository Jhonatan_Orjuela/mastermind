package com.expresionless.jokeapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;

/**
 * Splash screen used to be displayed before starting the application.
 * 
 * @author Jeff Buonamici, Tyler Patricio, Jhonatan Orjuela
 */
public class SplashActivity extends Activity {
	private int splashDuration = 3000;
	private static final String PREFS_NAME = "PrefsFile";
	private SharedPreferences prefs;
	private boolean isTouched = false;

	/**
	 * Creates the activity and sets the layout.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefs = getSharedPreferences(PREFS_NAME, 0);
		
		if (!prefs.getBoolean("splashScreen", true))
			splashDuration = 0;
		else
			setContentView(R.layout.splash);

		// create a handler object to display splash screen for
		// a few seconds
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// create the intent for the SplashActivity
				if (isTouched == false)
				{
					SplashActivity.this.startActivity(new Intent(
							SplashActivity.this, MainActivity.class));
					SplashActivity.this.finish();
				}
			}

		}, splashDuration);
	}
	
	/**
	 * Skips the SplashScreen when touched.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			isTouched = true;
			SplashActivity.this.startActivity(new Intent(
					SplashActivity.this, MainActivity.class));
			SplashActivity.this.finish();
		}
		
		return true;
	}
}